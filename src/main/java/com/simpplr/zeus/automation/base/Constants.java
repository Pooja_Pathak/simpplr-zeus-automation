package com.simpplr.zeus.automation.base;

public class Constants {
	
	
	public static final String browser = "chrome";
	public static final String objectRepo = "src/test/resources/config/objectRepo.properties";
	public static final String credentials = "src/test/resources/config/environment.properties";
	public static final String testsiteurl = "https://qa-automation-tenant.qa.simpplr.xyz/";
	public static long implicitwait=10;

}
