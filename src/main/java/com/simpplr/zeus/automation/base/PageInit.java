package com.simpplr.zeus.automation.base;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PageInit {

    public static WebDriver driver;
    private static final Logger logger = LogManager.getLogger(PageInit.class);


    public WebDriver initConfiguration() throws InterruptedException {
        try {
        	 BasicConfigurator.configure();
             System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/executables/chromedriver.exe");
             driver = new ChromeDriver();
             driver.manage().window().maximize();
             logger.info("typing from logger");
        } catch (Exception e) {
            e.printStackTrace();
            driver.quit();
        }
        return driver;
    }


}
