package com.simpplr.zeus.automation.page;

import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.simpplr.zeus.automation.util.ReadProperty;
import com.simpplr.zeus.automation.util.WebUtil;

public class LoginPage {

    private static final Logger logger = LogManager.getLogger(LoginPage.class);
    WebDriver driver;
    WebUtil webUtil;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        webUtil = new WebUtil(driver);
    }

    public By getUserNameTxtBox() throws IOException {
        return By.id(ReadProperty.propertyReader("userNameTxtBox"));
    }

    public By getPasswordTxtBox() throws IOException {
        return By.id(ReadProperty.propertyReader("passwordTxtBox"));
    }

    public By getLoginButton() throws IOException {
        return By.xpath(ReadProperty.propertyReader("loginButton"));
    }

    public void login(String uName, String pwd) {
        try {
            webUtil.enterTestData(getUserNameTxtBox(), uName);
            webUtil.enterTestData(getPasswordTxtBox(), pwd);
            webUtil.click(getLoginButton());
            webUtil.waitForElementNotPresent(getLoginButton(), 20);
            webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("avatar")));
            logger.info("Login credentials have been entered");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	public void clickOnAvatar() throws IOException {
		webUtil.click(By.xpath(ReadProperty.propertyReader("avatar")));
	}
	public int getManageTenantCountUnderProfileMenu() throws IOException {
		return webUtil.getElements(By.xpath(ReadProperty.propertyReader("ManageTenant"))).size();
	}


}
