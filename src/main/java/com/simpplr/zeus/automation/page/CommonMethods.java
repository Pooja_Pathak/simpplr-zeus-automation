package com.simpplr.zeus.automation.page;

import io.cucumber.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Random;

public class CommonMethods {
	
	static WebDriver driver;
	@SuppressWarnings("static-access")
	
	
	
	public CommonMethods(WebDriver driver) {
		this.driver = driver;
	    
	  }
	
	
	public static void click(WebElement element) {
      element.click();
		}

	public static void type(WebElement element, String value) {
	 
       element.sendKeys(value);
    }
	
	
	public static boolean elementIsPresent(WebElement element) {
		if(element.isDisplayed()) {
			return true;
		}
		return false;
	}
	
	public static void scrollDown() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
	}
	public static void waits() {
		
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            //Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }
	public String getTitle() {
		CommonMethods.waits();
	   		return driver.getTitle();
	   		
	   		}
	
	public static String getTextValue(WebElement element) {
		return element.getText();
	}

	public static void uploadFile(String path) throws AWTException {
		Robot robot = new Robot();
		StringSelection str = new StringSelection(path);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
		robot.delay(300);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(90);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}
	
	public static void clickEnter() throws AWTException {
		Robot robot = new Robot();
		
		robot.keyPress(KeyEvent.VK_ENTER);
		
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public static void dragAndDrop(WebElement source, WebElement target) {
		Actions act = new Actions(driver);
		act.dragAndDrop(source, target).release().build().perform();
		//act.clickAndHold(source).moveToElement(target).release(source).perform();
		//act.clickAndHold(source).moveToElement(target).dragAndDrop(source, target);
		//act.release().build().perform();
	}

	public static String getRandomString(){
		int leftLimit = 97; // letter 'a'
		int rightLimit = 122; // letter 'z'
		int targetStringLength = 10;
		Random random = new Random();
		StringBuilder buffer = new StringBuilder(targetStringLength);
		for (int i = 0; i < targetStringLength; i++) {
			int randomLimitedInt = leftLimit + (int)
					(random.nextFloat() * (rightLimit - leftLimit + 1));
			buffer.append((char) randomLimitedInt);
		}
		return buffer.toString();
	}
	public static void closeBrowser() {
		driver.close();
		driver.quit();
	}


}
