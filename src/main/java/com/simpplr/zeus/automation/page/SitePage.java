package com.simpplr.zeus.automation.page;

import com.simpplr.zeus.automation.util.ReadProperty;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import com.simpplr.zeus.automation.util.WebUtil;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class SitePage {
    private static final Logger logger = LogManager.getLogger(SitePage.class);
    WebDriver driver;
    WebUtil webUtil;

    public SitePage(WebDriver driver) {
        this.driver = driver;
        webUtil = new WebUtil(driver);
    }

    public Boolean verifyOnHeaderOne(String sites) throws IOException {
        String value = ReadProperty.propertyReader("h1Contains").replace("value", sites);
        return webUtil.waitForElementToBeVisible(By.xpath(value)).isDisplayed();
    }

    public boolean verifyOnSpan(String add_site) throws IOException {
        String value = ReadProperty.propertyReader("spanContains").replace("value", add_site);
        return webUtil.waitForElementToBeVisible(By.xpath(value)).isDisplayed();
    }

    public boolean verifyOnDT(String param) throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("parameterOnPreview").replace("value", param))).isDisplayed();
    }

    public int VerifyCategoryList() throws IOException {
        webUtil.scrollTillElement(webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("categories"))));
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader("listOfCategory"))).size();
    }

    public int getTheSpanCount(String add_site) throws IOException {
        String value = ReadProperty.propertyReader("spanContains").replace("value", add_site);
        return webUtil.getElements(By.xpath(value)).size();
    }

    public void clickOnSpan(String button) throws IOException {
        String value = ReadProperty.propertyReader("spanContains").replace("value", button);
        webUtil.click(By.xpath(value));
    }

    public void sendCategoriesName(String categoriesNameValue) throws IOException {
        logger.info("Category value passed : " + categoriesNameValue);
        WebElement categoryAction = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("category")));
        categoryAction.click();
        categoryAction.sendKeys(categoriesNameValue);
        categoryAction.sendKeys(Keys.ENTER);
    }

    public void sendCategoriesOnPage(String categoriesNameValue) throws IOException {
        logger.info("Category value passed : " + categoriesNameValue);
        WebElement categoryAction = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("categoryOnPage")));
        categoryAction.click();
        categoryAction.sendKeys(categoriesNameValue);
        categoryAction.sendKeys(Keys.ENTER);
    }

    public void sendSiteName(String siteNameValue) throws IOException {
        webUtil.type(By.id(ReadProperty.propertyReader("siteName")), siteNameValue);
    }

    public void selectAccessOrContent(String access) throws IOException {
        String value = ReadProperty.propertyReader("accessOnContent").replace("value", access);
        webUtil.click(By.xpath(value));
    }

    public void clickOnButton(String button) throws IOException {
        String value = ReadProperty.propertyReader("buttonContains").replace("value", button);
        webUtil.click(By.xpath(value));
    }

    public boolean VerifyCarouselPopUp() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("carouselPopUp"))).isDisplayed();
    }

    public String verifyCancelPopUp() {
        return driver.switchTo().alert().getText();
    }

    public void clickOnSiteLinkWithIndex(String index) throws IOException {
        String value = ReadProperty.propertyReader("siteWithIndex").replace("1", index);
        webUtil.click(By.xpath(value));

    }

    public String getTheSiteNameWithIndex(String index) throws IOException {
        String value = ReadProperty.propertyReader("siteWithIndex").replace("1", index);
        return webUtil.waitForElementToBeVisible(By.xpath(value)).getText();

    }

    public void uploadCoverImage(String path) throws IOException, AWTException {
        webUtil.clickUsingJavaScript(By.xpath(ReadProperty.propertyReader("coverImageOption")));
        String value = ReadProperty.propertyReader("spanContains").replace("value", "Replace cover image");
        String value1 = ReadProperty.propertyReader("spanContains").replace("value", "Upload cover image");
        List<WebElement> imageElement = webUtil.getElements(By.xpath(value));
        if (imageElement.size() == 0) {
            webUtil.clickUsingJavaScript(By.xpath(value1));
        } else {
            webUtil.clickUsingJavaScript(By.xpath(value));
        }

        attachFile(path);
    }

    public void uploadCoverImageRemove(String path) throws IOException, AWTException {
        webUtil.clickUsingJavaScript(By.xpath(ReadProperty.propertyReader("coverImageOption")));
        String value = ReadProperty.propertyReader("buttonContains").replace("value", "Remove cover image");
        String value1 = ReadProperty.propertyReader("spanContains").replace("value", "Upload cover image");
        List<WebElement> imageElement = webUtil.getElements(By.xpath(value));
        if (imageElement.size() != 0) {
            webUtil.clickUsingJavaScript(By.xpath(value));
            attachFile(path);
            webUtil.clickUsingJavaScript(By.xpath(ReadProperty.propertyReader("coverImageOption")));
        }
        webUtil.clickUsingJavaScript(By.xpath(value1));
        attachFile(path);
    }

    public void removeCoverImage(String path) throws IOException, AWTException {
        webUtil.clickUsingJavaScript(By.xpath(ReadProperty.propertyReader("coverImageOption")));
        String value = ReadProperty.propertyReader("buttonContains").replace("value", "Remove cover image");
        String value1 = ReadProperty.propertyReader("spanContains").replace("value", "Upload cover image");
        List<WebElement> imageElement = webUtil.getElements(By.xpath(value));
        if (imageElement.size() == 0) {
            webUtil.clickUsingJavaScript(By.xpath(value1));
            attachFile(path);
        }
        webUtil.clickUsingJavaScript(By.xpath(value));

    }

    public int verifyOnCoverUploadImage() throws IOException {
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader("coverImagUpload"))).size();
    }

    public void selectAnyPage(String i) throws IOException {
        String value = ReadProperty.propertyReader("anyPageFromSite").replace("1",i);
        webUtil.click(By.xpath(value));
    }

    public String checkLikeOrDislike() throws IOException {
        String likeDislike = "Like";
        String titleValue = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("likeDislikeButton"))).getAttribute("title");
        if(titleValue.contains("Unlike")) {
            likeDislike =  "Unlike";
        }
        return likeDislike;
    }

    public void clickOnLikeDislikeButton() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("likeDislikeButton")));
    }

    public void clickOnManageDashboard() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("manageDashboard")));
    }

    public String getTextForFirstCarousel() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("firstCarousel"))).getText();
    }

    public void cancelFirstCarousel() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("firstCancel")));
    }

    public void sendOnSearchCarousel(String carouselName) throws IOException, InterruptedException {
        WebElement categoryAction = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("searchCarousel")));
        categoryAction.click();
        categoryAction.sendKeys(carouselName);
        categoryAction.sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        String value = ReadProperty.propertyReader("divContains").replace("1",carouselName);
        webUtil.clickUsingJavaScript(By.xpath(value));
        categoryAction.sendKeys(Keys.ENTER);
        Thread.sleep(2000);
    }

    public boolean verifyCarouselChanges(String carouselName) throws IOException {
        String value = ReadProperty.propertyReader("carouselChanges").replace("value",carouselName);
        return webUtil.waitForElementToBeVisible(By.xpath(value)).isDisplayed();
    }

    public void selectCarouselType(String type) throws IOException {
        logger.info("Carousel Type is : "+type);
        String value = ReadProperty.propertyReader("selectCarousel").replace("value",type);
        webUtil.scrollTillElement(webUtil.waitForElementToBePresent(By.xpath(value)));
        webUtil.click(By.xpath(value));
    }

    public boolean verifySelectedCarouselLayout(String type) throws IOException {
        logger.info("Carousel Type : "+type);
        if(type.equals("Showcase")) {
            return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("showcasePath"))).isDisplayed();
        }else if(type.equals("Standard")) {
            return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("standardPath"))).isDisplayed();
        }else {
            logger.info("Invalid carousel name");
            return false;
        }
    }

    public void clickOnAddContentOnSite() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("addContentOnSite")));
    }

    public boolean verifyAddSiteLabels(String text) throws IOException {
        String value = ReadProperty.propertyReader("addContentType").replace("value",text);
        return webUtil.waitForElementToBeVisible(By.xpath(value)).isDisplayed();
    }

    public void clickAddSiteLabels(String text) throws IOException {
        String value = ReadProperty.propertyReader("addContentType").replace("value",text);
        webUtil.click(By.xpath(value));
    }

    public void chooseSite(String site) throws IOException {
        webUtil.click(By.id(ReadProperty.propertyReader("chooseSite")));
        chooseSiteFromDropDown(site);

    }

    public boolean verifyAddPage(String siteName) throws IOException {
        String value = ReadProperty.propertyReader("addPage").replace("value",siteName);
        webUtil.waitForElementToBePresent(By.xpath(value));
        return webUtil.waitForElementToBeVisible(By.xpath(value)).isDisplayed();
    }

    public void selectContentType(String contentType) throws IOException {
        String value = ReadProperty.propertyReader("contentType").replace("value",contentType);
        webUtil.scrollTillElement(webUtil.waitForElementToBeVisible(By.xpath(value)));
        webUtil.click(By.xpath(value));
    }

    public void sendContentTitle(String textArea, String pageValue) throws IOException {
        logger.info("Page Value : "+pageValue);
        String value = ReadProperty.propertyReader("titleNameForContent").replace("value",textArea);
        webUtil.type(By.xpath(value), pageValue);
    }

    public void sendAddContent(String addContent) throws IOException {
        logger.info("Add Content : "+addContent);
        webUtil.type(By.xpath(ReadProperty.propertyReader("addContentText")), addContent);
    }


    public void clickOnLabel(String label) throws IOException {
        String value = ReadProperty.propertyReader("labelContains").replace("value",label);
        webUtil.click(By.xpath(value));
    }

    public void attachFile(String path) throws AWTException {
        logger.info("upload file path : "+path);
        CommonMethods.uploadFile(path);
    }

    public boolean verifyUploadedImageOnPage() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("pageAddImageCaption"))).isDisplayed();
    }

    public void clickOnAdd() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("addButtonOnContentPage")));
    }

    public void sendLocation(String loc) throws IOException {
        webUtil.type(By.id(ReadProperty.propertyReader("location")), loc);
    }

    public boolean verifyDisplayImage() throws IOException {
        boolean image = false;
        List<WebElement> imageElement = webUtil.getElements(By.xpath(ReadProperty.propertyReader("displayImage")));
        List<WebElement> uploadElement = webUtil.getElements(By.xpath(ReadProperty.propertyReader("uploadImage")));
        if(imageElement.size() > 0) {
            image = true;
            logger.info("Display image is there");
        }
        else if(uploadElement.size() > 0) {
            logger.info("Add a site image");
            image = true;
        }
        return image;
    }

    public void uploadDisplayImage(String path) throws IOException, AWTException, InterruptedException {
        List<WebElement> imageElement = getUploadImageElement();
        if(imageElement.size() > 0) {
            logger.info("Display image is there");
            Thread.sleep(2000);
            clickOnUploadSiteImage();
            clickOnButton("Remove site image");
        }
        else{
            logger.info("Add a site image");
        }
        webUtil.click(By.xpath(ReadProperty.propertyReader("uploadImage")));
        attachFile(path);
    }

    public void uploadDisplayImageReplace(String path) throws IOException, AWTException, InterruptedException {
        List<WebElement> imageElement = getUploadImageElement();
        if (imageElement.size() <= 0) {
            logger.info("Add a site image");
            webUtil.click(By.xpath(ReadProperty.propertyReader("uploadImage")));
        }
        logger.info("Display image is there");
        Thread.sleep(2000);
        clickOnUploadSiteImage();
        clickOnSpan("Replace site image");
        attachFile(path);
    }

    private List<WebElement> getUploadImageElement() throws IOException {
        List<WebElement> imageElement = webUtil.getElements(By.xpath(ReadProperty.propertyReader("uploadSiteImage")));
        logger.info("uploaded imaged : "+imageElement.size());
        return imageElement;
    }


    public void removeSiteImage(String path) throws IOException, AWTException, InterruptedException {
        List<WebElement> imageElement = getUploadImageElement();
        if (imageElement.size() <= 0) {
            logger.info("Add a site image");
            webUtil.click(By.xpath(ReadProperty.propertyReader("uploadImage")));
            attachFile(path);
        }
        logger.info("Display image is there");
        Thread.sleep(2000);
        clickOnUploadSiteImage();
        clickOnButton("Remove site image");
        attachFile(path);
    }


    public void clickOnUploadSiteImage() throws IOException {
        webUtil.clickUsingJavaScript(By.xpath(ReadProperty.propertyReader("uploadSiteImage")));
    }

    public boolean verifyTabActivated(String tab) throws IOException {
        String value = ReadProperty.propertyReader("aContains").replace("value", tab);
        String text = webUtil.waitForElementToBeVisible(By.xpath(value)).getAttribute("class");
        return text.contains("is-active");
    }

    public int getTheCountOfCategoriesOnCategoryPage() throws IOException {
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader("categoriesOnFeatured"))).size();
    }

    public int getTheCountOfMySiteList() throws IOException {
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader("listOfSitesOnMySite"))).size();
    }

    public int getTheCountOwnerOnSite() throws IOException {
        String value = ReadProperty.propertyReader("divContains").replace("1","Owner");
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader(value))).size();
    }

    public boolean checkImageRemoved() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("uploadImage"))).isDisplayed();
    }

    public void clickOnCategoryViewAll() throws IOException {
        webUtil.scrollTillElement(webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("categories"))));
        webUtil.click(By.xpath(ReadProperty.propertyReader("categoryViewAll")));
    }

    public int VerifyCategoryListOnCategoryPage() throws IOException {
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader("categoryHeader"))).size();
    }

    public String VerifyDuplicateSite() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("errorMessage"))).getText();
    }

    public boolean verifyFeaturedPage() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("featuredPage"))).isDisplayed();
    }

    public List<String> recentlyVisitedSitesCount() throws IOException, InterruptedException {
        Thread.sleep(3000);
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("recentlyVisitedCount")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("recentlyVisitedCount")));
        List<String> siteNameText = new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public List<String> getListOfSiteOnAllSitePage() throws IOException {
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("listOfSiteOnAllSite")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("listOfSiteOnAllSite")));
        List<String> siteNameText = new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public List<String> getListOfSiteOnLatestPage() throws IOException {
        webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("siteListOnLatestPage")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("siteListOnLatestPage")));
        List<String> siteNameText = new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public List<String> getListOfFeatureCardName() throws IOException {
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("listOfFeatureCard")));
        List<WebElement> cardName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("listOfFeatureCard")));
        List<String> cardNameText =  new LinkedList<>();
        for (WebElement webElement : cardName) {
            cardNameText.add(webElement.getText());
        }
        return cardNameText;
    }

    public void sendFeatureSiteNameAndSelect(String siteName) throws IOException, InterruptedException {
        WebElement featuredSite = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("featuredSearchSite")));
        featuredSite.click();
        featuredSite.sendKeys(siteName);
        featuredSite.sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        String value = ReadProperty.propertyReader("divContains").replace("1", siteName);
        webUtil.clickUsingJavaScript(By.xpath("("+value+")[2]"));
        featuredSite.sendKeys(Keys.ENTER);
        webUtil.hitKey(Keys.ENTER);
    }

    public boolean verifyFeaturedCardName(String siteName) throws IOException {
        String value = ReadProperty.propertyReader("featureCardName").replace("value", siteName);
        return webUtil.waitForElementToBeVisible(By.xpath(value)).isDisplayed();
    }

    public boolean verifyFeaturedCareCategory(String siteName) throws IOException {
        String value = ReadProperty.propertyReader("featuredCategory").replace("value", siteName);
        return webUtil.waitForElementToBeVisible(By.xpath(value)).isDisplayed();
    }

    public boolean verifyFeaturedSiteMember(String siteName) throws IOException {
        String value = ReadProperty.propertyReader("featuredMember").replace("value", siteName);
        return webUtil.waitForElementToBeVisible(By.xpath(value)).isDisplayed();
    }

    public boolean verifyFeaturedCareOwnerOrFollowing(String siteName) throws IOException {
        String spanValue = ReadProperty.propertyReader("featuredFollowSpan").replace("value", siteName);
        String buttonValue = ReadProperty.propertyReader("featuredFollowButton").replace("value", siteName);
        List<WebElement> span = webUtil.getElements(By.xpath(spanValue));
        if(span.size()==0) {
            return webUtil.waitForElementToBeVisible(By.xpath(buttonValue)).isDisplayed();
        }
        return span.get(0).isDisplayed();
    }

    public void sendFeatureSiteName(String siteName) throws IOException, InterruptedException {
        WebElement featuredSite = webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("featuredSearchSite")));
        featuredSite.click();
        featuredSite.sendKeys(siteName);
        featuredSite.sendKeys(Keys.ENTER);
        Thread.sleep(2000);
    }

    public boolean verifyInvalidFeaturedSite() throws IOException {
        String value = ReadProperty.propertyReader("divContains").replace("1","No results");
        return webUtil.waitForElementToBePresent(By.xpath(value)).isDisplayed();
    }

    public void clickOnAnySiteFromRecentlyVisitedOnFeatured(String siteName) throws IOException {
        String value = ReadProperty.propertyReader("featuredSites").replace("value",siteName);
        webUtil.click(By.xpath(value));
    }

    public void clickOnCategoryOnFeatureCard() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("categoryOnFeature")));
    }

    public void clickOnMembersOnFeature() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("membersOnFeature")));
    }

    public boolean verifyAboutOnSite() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("aboutOnSite"))).isDisplayed();
    }
    public int verifyFollowButton() throws IOException {
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader("followButton"))).size();
    }

    public int getTheCountOfEntryOnPage() throws IOException {
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader("pagination"))).size();
    }

    public void searchSiteOnFollowingTab(String siteName) throws IOException {
        WebElement categoryAction = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("searchFollowing")));
        webUtil.scrollTillElement(categoryAction);
        categoryAction.clear();
        categoryAction.sendKeys(siteName);
        categoryAction.sendKeys(Keys.ENTER);
    }

    public void typeCategoryName(String siteNameValue) throws IOException {
        webUtil.type(By.xpath(ReadProperty.propertyReader("enterCategoryName")), "");
        webUtil.type(By.xpath(ReadProperty.propertyReader("enterCategoryName")), siteNameValue);
    }

    public List<String> getMembersList() throws IOException {
        webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("memberList")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("memberList")));
        List<String> siteNameText = new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public List<String> getFeaturedList() throws IOException {
        webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("featureSiteName")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("featureSiteName")));
        List<String> siteNameText = new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public void clickOnCancelButton() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("cancelButtonOnAddSite")));
    }

    public void dragAndDropFeaturedCard() throws IOException {
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("dragDropFeature")));
        List<WebElement> myList=webUtil.getElements(By.xpath(ReadProperty.propertyReader("dragDropFeature")));
        Actions act = new Actions(driver);
        act.dragAndDrop(myList.get(1), myList.get(2)).build().perform();
    }

    public void dropDownAccess(String access) throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("publicAccessDropDownOnSite").replace("value", access)));
    }

    public String getAccessSiteName(String access) throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("accessSiteName").replace("value",access))).getText();
    }

    public boolean verifySiteAccess(String siteName, String access) throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("privateAccess").replace("value", siteName).replace("access",access))).isDisplayed();
    }

    public void clickOnDeactivatedOption() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("deactivatedDropDown")));
    }

    public String getTheSiteNameForDeactivated() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("deactivateSiteName"))).getText();
    }

    public int getTheUploadImageCount() throws IOException {
       return webUtil.getElements(By.xpath(ReadProperty.propertyReader("uploadSiteImage"))).size();
    }

    public boolean compareLatestDate() throws IOException, ParseException {
        String first = webUtil.getElement(By.xpath(ReadProperty.propertyReader("dateFeedPosts"))).getText().substring(0,12);
        String second = webUtil.getElement(By.xpath(ReadProperty.propertyReader("dateFeedPosts").replace("1","2"))).getText().substring(0,12);
        Date date1=new SimpleDateFormat("MMM dd, yyyy").parse(first);
        Date date2=new SimpleDateFormat("MMM dd, yyyy").parse(second);
        return  date1.after(date2);
    }

    public void clickOnAddContentOnHome() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("contentAdd")));
    }

    public void chooseSiteFromDropDown(String site) throws IOException {
        logger.info("Site Name selected : "+site);
        WebElement siteName = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("inputSiteNameOnContent")));
        siteName.click();
        siteName.sendKeys(site);
        siteName.sendKeys(Keys.ENTER);
        String value = ReadProperty.propertyReader("divContains").replace("1",site);
        webUtil.click(By.xpath(value));
        webUtil.click(By.xpath(value));
        webUtil.hitKey(Keys.ENTER);
    }

    public void clickOnFollowButton(String siteName) throws IOException {
        verifyOnHeaderOne(siteName);
        int size = verifyFollowButton();
        clickOnSpan("Follow");
        if(size==0){
            clickOnButton("Unfollow site");
        }
        clickOnButton("Follow site");
    }

    public void clickOnUnfollowButton(String siteName) throws IOException {
        verifyOnHeaderOne(siteName);
        int size = verifyFollowButton();
        clickOnSpan("Follow");
        if(size!=0){
            clickOnButton("Follow site");
        }
        clickOnButton("Unfollow site");
    }

    public void clickOnRequested() throws IOException {
        int size = getTheSpanCount("Request membership");
        if(size!=0){
            clickOnSpan("Request membership");
        }
    }

    public List<String> getListOfButton() throws IOException {
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("buttonOnFiles")));
        List<WebElement> button = webUtil.getElements(By.xpath(ReadProperty.propertyReader("buttonOnFiles")));
        List<String> buttonText = new LinkedList<>();
        for (WebElement webElement : button) {
            buttonText.add(webElement.getText());
        }
        return buttonText;
    }

    public List<String> getTheUSerName() throws IOException {
        webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("nameOfUser")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("nameOfUser")));
        List<String> siteNameText = new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public void clickOnMemberRadioButton() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("memberId")));
    }
}
