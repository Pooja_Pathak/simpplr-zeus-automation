package com.simpplr.zeus.automation.page;

import com.simpplr.zeus.automation.util.ReadProperty;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.simpplr.zeus.automation.util.WebUtil;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;

public class DashboardPage {

	private static final Logger logger = LogManager.getLogger(DashboardPage.class);
	WebDriver driver;
	WebUtil webUtil;

	public DashboardPage(WebDriver driver) {
		this.driver = driver;
		webUtil = new WebUtil(driver);
	}

	public boolean verifyAddTileIconOnHomePage() throws IOException {
		return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("addTilePageOptionMenu")))
				.isDisplayed();
	}

	public boolean verifyManageDashboardIcon() throws IOException {
		return webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("manageDashboard")))
				.isDisplayed();
	}

	public void clickOnAddTile() throws IOException {
		webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("addTilePageOptionMenu")));
		webUtil.click(By.xpath(ReadProperty.propertyReader("addTilePageOptionMenu")));
	}

	public boolean verifyContentButton() throws IOException {
		return webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("contentButton"))).isDisplayed();
	}

	public boolean verifyAddTilePopUp() throws IOException {
		return webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("addTilePopUp"))).isDisplayed();
	}

	public void clickOnP(String button) throws IOException {
		String value = ReadProperty.propertyReader("pContains").replace("value", button);
		webUtil.click(By.xpath(value));
	}

	public void clickOnTileTitle(String title) throws IOException {
		webUtil.type(By.id(ReadProperty.propertyReader("tileTitle")), "");
		webUtil.type(By.id(ReadProperty.propertyReader("tileTitle")), title);
	}

	public boolean verifyCreatedTile(String tileName) throws IOException {
		String value = ReadProperty.propertyReader("hTwoContains").replace("value", tileName);
		return webUtil.waitForElementToBePresent(By.xpath(value)).isDisplayed();
	}

	public int getSiteTitle(String siteName) throws IOException {
		String value = ReadProperty.propertyReader("hTwoContains").replace("value", siteName);
		return webUtil.getElements(By.xpath(value)).size();
	}

	public void enableFeed() throws IOException, InterruptedException {
		String feedTab = ReadProperty.propertyReader("divContains").replace("1", "Feed");
		int feed = webUtil.getElements(By.xpath(feedTab)).size();
		if (feed == 0) {
			webUtil.click(By.xpath(ReadProperty.propertyReader("manageDashboard")));
			webUtil.click(By.id(ReadProperty.propertyReader("feedEnabled")));
			webUtil.click(By.id(ReadProperty.propertyReader("dashboardLayout")));
			Thread.sleep(2000);
		}
		webUtil.click(By.xpath(feedTab));
		logger.info("Clicked on feed Tab");
	}

	public void clickOnFavorite() throws IOException {
		webUtil.click(By.xpath(ReadProperty.propertyReader("feedFavorite")));
	}

	public int getCountOfFavorite() throws IOException {
		int count = webUtil.getElements(By.xpath(ReadProperty.propertyReader("feedFavorite"))).size();
		return count;
	}

	public String clickOnLikeButton(String i) throws IOException {
		String like = ReadProperty.propertyReader("linkDisLike").replace("1", i);
		String status = ReadProperty.propertyReader("LikeDislikeStatus").replace("1", i);
		webUtil.click(By.xpath(like));
		return webUtil.getElement(By.xpath(status)).getText();
	}

	public boolean verifyLikeDislike(String contains) throws IOException {
		String like = ReadProperty.propertyReader("checkTheStatus").replace("value", contains);
		return webUtil.waitForElementToBeVisible(By.xpath(like)).isDisplayed();
	}

	public boolean verifyReplyEditor(String i) throws IOException {
		String editor = ReadProperty.propertyReader("textEditor").replace("1", i);
		return webUtil.waitForElementToBeVisible(By.xpath(editor)).isDisplayed();
	}

	public void sendReplyEditor(String text, String i) throws IOException {
		String editor = ReadProperty.propertyReader("textEditor").replace("1", i);
		webUtil.type(By.xpath(editor), text);
	}

	public boolean verifyFeedPosted(String postText) throws IOException {
		String message = ReadProperty.propertyReader("checkReply").replace("message", postText);
		return webUtil.waitForElementToBeVisible(By.xpath(message)).isDisplayed();
	}

	public void clickOnSetting() throws IOException {

		webUtil.getElement(By.xpath(ReadProperty.propertyReader("manageDashboard"))).click();

	}

	public void selectLayout() throws IOException {
		List<WebElement> layouts = webUtil.getElements(By.xpath(ReadProperty.propertyReader("layoutList")));
		for (WebElement layOut : layouts) {
			if (layOut.isSelected()) {
				continue;
			}
			else {
				layOut.click();
				break;
			}
		}

	}

	public void clickDone() throws IOException {
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("layoutDoneButton"))).click();

	}

	

	public void clickOnAddPageEvents() throws IOException {
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("pageEventAndAlbum"))).click();

	}
	
	public void clickOnAddPageEventsBlogPost() throws Exception {
		Thread.sleep(3000);
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("pageEventAndAlbumBlogPost"))).click();

	}

	public void clickOnAddtoSiteDashboard() throws IOException {
		webUtil.scrollTillElement(webUtil.getElement(By.xpath(ReadProperty.propertyReader("addToSitedashboard"))));
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("addToSitedashboard"))).click();

	}

	

	public void dragdropTile() throws IOException {
		List<WebElement> tilesOnPage = webUtil.getElements(By.xpath(ReadProperty.propertyReader("tileList")));
		CommonMethods.dragAndDrop(tilesOnPage.get(0), tilesOnPage.get(1));
	}

	public void clickOnOptionsOnTile() throws Exception {
		Thread.sleep(3000);
		List<WebElement> editOptions = webUtil.getElements(By.xpath(ReadProperty.propertyReader("tileOptionsList")));
		editOptions.get(0).click();

	}

	public void clickOnRemoveOption() throws IOException {
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("removeTile"))).click();

	}

	public void clickOnRemovePopup() throws IOException {
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("removePopup"))).click();

	}

	public boolean validateFlashMessage(String message) throws IOException {
		webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("divContains").replace("1", message)));
		if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("divContains").replace("1", message))).size() > 0) {
		
			return true;
		}
		return false;
	}

	public void selectPagesContent() throws IOException {
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("pagesTile"))).click();

	}

	public void selectPagesCategory() {
		try {
			webUtil.getElement(By.xpath(ReadProperty.propertyReader("pagesCategory"))).click();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void selectcategoryUnderPagecategory() {

		try {
			webUtil.selectByValue(By.xpath(ReadProperty.propertyReader("categoryUnderPageCategory")), "Uncategorized");
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void SelectStarIcon() throws IOException {
    boolean flag=webUtil.getElements(By.xpath(ReadProperty.propertyReader("starIconisSelected"))).size()>0 ;
		if (!flag) {
			webUtil.getElement(By.xpath(ReadProperty.propertyReader("starIcon"))).click();
		}

	}
	public void UnSelectStarIcon() throws IOException {
		boolean flag=webUtil.getElements(By.xpath(ReadProperty.propertyReader("starIconisSelected"))).size()>0 ;
		if (flag) {
		
			webUtil.getElement(By.xpath(ReadProperty.propertyReader("starIcon"))).click();
		}

	}
	public boolean validateStarIsSelected() {
		try {
			if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("starIconisSelected"))).size()>0) {
				return true;
			} else if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("starIconisNotSelected"))).size()>0) {
				return false;
			}
			return false;
		} catch (IOException e) {

			e.printStackTrace();
		}
		return false;
	}

	public void clickSiteCategory() {
		try {
			webUtil.getElement(By.xpath(ReadProperty.propertyReader("siteAndcategories"))).click();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enterSiteCategoryDetails(String category) throws IOException {
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("textboxSiteNCategories"))).sendKeys(category);
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("textboxSiteNCategories"))).sendKeys(Keys.ENTER);

	}

	public boolean validateSiteCategory(String category) {
		List<WebElement> categoryList = webUtil.getElements(By.xpath("//a[text()='" + category + "']"));
		if (categoryList.get(0).getText().equalsIgnoreCase(category)) {
			return true;
		} else
			return false;
	}

	public boolean validateEmptyCarousel() throws IOException {
		if (webUtil.getElement(By.xpath(ReadProperty.propertyReader("emptyCarousel"))).getText()
				.equalsIgnoreCase("This carousel is empty")) {
			return true;
		}
		return false;
	}

	public void clickManageCaroUsel() throws IOException {
		CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("manageDashboard"))));

	}

	public void enterSearchContent(String content) {
		try {
			
			 WebElement categoryAction = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("searchCarousel")));
		        categoryAction.click();
		        categoryAction.sendKeys(content);
		        categoryAction.sendKeys(Keys.ENTER);
		        Thread.sleep(2000);
			
		        //WebElement myElem=webUtil.getElement(By.xpath(ReadProperty.propertyReader("searchCarousel")));
		
			
			
			//webUtil.getElement(By.xpath(ReadProperty.propertyReader("searchCarousel"))).clear();
			

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public void clickDoneCarousel() {
		try {
			CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("doneCarousel"))));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void clickOnHomeButton()  {
		

		try {
			Thread.sleep(2000);
			webUtil.scrollTillElement(webUtil.getElement(By.xpath(ReadProperty.propertyReader("addToHome"))));
			Thread.sleep(2000);
			CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("addToHome"))));
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void selectSiteCategoryTab() {

		try {
			CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("siteCategoriesTab"))));
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void clickSitecategoryOptions() throws IOException {
		//webUtil.scrollVerticallyDown();
		if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("siteOptionsList"))).size() > 0) {
			List<WebElement> listofSitecategories = webUtil.getElements(By.xpath(ReadProperty.propertyReader("siteOptionsList")));
			//webUtil.scrollTillElement(listofSitecategories.get(0));
			CommonMethods.click(listofSitecategories.get(0));
		}
	}

	public void clickSitecategoryOptionER(String option) {

		if (option.equalsIgnoreCase("Edit")) {
			try {
				Thread.sleep(3000);
				CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("siteEditOption"))));
			} catch (Exception e) {

				e.printStackTrace();
			}
		} else if (option.equalsIgnoreCase("Remove")) {
			try {
				Thread.sleep(3000);
				CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("siteRemoveOption"))));
			} catch (Exception e) {

				e.printStackTrace();
			}

		}
	}

	public void clickOnEditSave() {
		try {
			webUtil.scrollTillElement(webUtil.getElement(By.xpath(ReadProperty.propertyReader("siteEditSave"))));
			CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("siteEditSave"))));
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public boolean validateSiteCategoryRemoved() {
		try {
			if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("siteOptionsList"))).size() == 0) {
				return true;
			}
		} catch (IOException e) {

			e.printStackTrace();
		}
		return false;

	}

	public boolean validateSiteCategoryEdited() {

		try {
			if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("siteEditMessage"))).size() > 0) {
				return true;
			}
		} catch (IOException e) {

			e.printStackTrace();
		}
		return false;
	}

	public void enterSiteCategoryTitle(String title) {
		try {
		
			webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("siteCategoryTitle")));
			
			CommonMethods.type(webUtil.getElement(By.xpath((ReadProperty.propertyReader("siteCategoryTitle")))), title);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public boolean validateAddedContent() {
		try {
			Thread.sleep(2000);
		
			webUtil.scrollVerticallyDown();
			webUtil.scrollTillElement(webUtil.getElement(By.xpath(ReadProperty.propertyReader("homeTab"))));
			
			
			if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("contentOptionsList"))).size() > 0) {
				return true;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return false;

	}

	public boolean validateDeletedContent() {
		try {
			Thread.sleep(2000);
			if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("contentOptionsList"))).size() == 0) {
				return true;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return false;

	}

	public void clickOnContentOptions() throws IOException {
		//webUtil.scrollVerticallyDown();
		if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("contentOptionsList"))).size() > 0) {
			List<WebElement> listofSitecategories = webUtil.getElements(By.xpath(ReadProperty.propertyReader("contentOptionsList")));
			CommonMethods.click(listofSitecategories.get(0));
		}

	}

	public boolean validateSiteCategoryAdded() throws Exception {
		
		 driver.get(driver.getCurrentUrl());
		 Thread.sleep(5000);
		 webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("siteTesting")));
		 System.out.println(webUtil.getElements(By.xpath("siteTesting")).size());
		 webUtil.scrollTillElement(webUtil.getElements(By.xpath("siteTesting")).get(0));
		
		if(webUtil.getElements(By.xpath("siteTesting")).size()>0) {
			return true;
		}
		return false;
	}

	public void clickOnCarouselOptions() throws IOException {
		webUtil.scrollTillElement(webUtil.getElement(By.xpath(ReadProperty.propertyReader("carouselOptions"))));
		CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("carouselOptions"))));
		
	}

	public void clickOnStartAutoPlay() throws IOException {
		webUtil.scrollTillElement(webUtil.getElement(By.xpath(ReadProperty.propertyReader("carouselStartAutoPlay"))));
		CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("carouselStartAutoPlay"))));
		
		
	}

	public void clickOnStopAutoPlay() throws IOException {
		
		webUtil.scrollTillElement(webUtil.getElement(By.xpath(ReadProperty.propertyReader("carouselStopAutoPlay"))));
		CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("carouselStopAutoPlay"))));
		
	}

	public void clickEditCarousel() throws IOException {
		webUtil.scrollTillElement(webUtil.getElement(By.xpath(ReadProperty.propertyReader("carouselEdit"))));
		CommonMethods.click(webUtil.getElement(By.xpath(ReadProperty.propertyReader("carouselEdit"))));
		
	}

	public void dragCarouselContent() throws IOException {
	if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("carouselContentDelete"))).size() > 0) {
		List<WebElement> listofSitecategories = webUtil.getElements(By.xpath(ReadProperty.propertyReader("carouselContentDelete")));
		
		Actions act = new Actions(driver);
	    act.dragAndDrop(listofSitecategories.get(0), listofSitecategories.get(1)).build().perform();
	}
	}

	public void removeAllCaroselContent() throws Exception {
		Thread.sleep(2000);
		if (webUtil.getElements(By.xpath(ReadProperty.propertyReader("carouselContentDelete"))).size() > 0) {
			List<WebElement> listofSitecategories = webUtil.getElements(By.xpath(ReadProperty.propertyReader("carouselContentDelete")));
			for(WebElement cross:listofSitecategories) {
			CommonMethods.click(cross);
			}
		}
		
	}
}
