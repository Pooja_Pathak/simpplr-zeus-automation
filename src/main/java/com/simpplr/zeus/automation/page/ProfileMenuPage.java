package com.simpplr.zeus.automation.page;

import com.simpplr.zeus.automation.util.ReadProperty;
import com.simpplr.zeus.automation.util.WebUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class ProfileMenuPage {

    private static final Logger logger = LogManager.getLogger(SitePage.class);
    WebDriver driver;
    WebUtil webUtil;

    public ProfileMenuPage(WebDriver driver) {
        this.driver = driver;
        webUtil = new WebUtil(driver);
    }

    public void clickOnDivTab(String subTab) throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("profileMenu").replace("value",subTab)));
    }

    public void clickOnAddPerson() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("addButtonOnAddPeople")));
    }

    public boolean verifyManageSitePage() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("manageSitePage"))).isDisplayed();
    }

    public List<String> getListOfSiteCategory() throws IOException {
        webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("siteCategoryList")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("siteCategoryList")));
        List<String> siteNameText =  new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public boolean verifyAZFilter() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("aZFilter"))).isDisplayed();
    }

    public void sendCategoryName(String catName) throws IOException {
        logger.info("category name : "+logger);
        webUtil.type(By.id(ReadProperty.propertyReader("categoryName")), "");
        webUtil.type(By.id(ReadProperty.propertyReader("categoryName")), catName);
    }

    public boolean verifyCategory(String text) throws IOException {
        String value = ReadProperty.propertyReader("siteCategoryName").replace("value",text);
        return webUtil.waitForElementToBeVisible(By.xpath(value)).isDisplayed();
    }

    public void searchCategoryOnSiteCategory(String text) throws IOException {
        logger.info("search category name : "+text);
        WebElement categoryAction = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("searchCategory")));
        categoryAction.clear();
        categoryAction.sendKeys(text);
        categoryAction.sendKeys(Keys.ENTER);
    }

    public boolean verifyNothingToShow() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("noResult"))).isDisplayed();
    }

    public void clickOnClearSearch() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("clearSearch")));
    }

    public void clickOnDropFromDeleteAndEdit() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("dropFromDeleteAndEdit")));
    }

    public void clickOnDropWithValue(String value) throws IOException {
        String text = ReadProperty.propertyReader("dropDownOnSiteCategory").replace("value",value);
        webUtil.click(By.xpath(text));
    }

    public String getCategoryValue() throws IOException {
        return webUtil.waitForElementToBePresent(By.id(ReadProperty.propertyReader("categoryName"))).getAttribute("value");
    }

    public void clickOnSaveButton() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("saveButton")));
    }

    public List<String> getListOfCategoryName() throws IOException {
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("listOfCatOnManageSite")));
        List<String> siteNameText =  new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public void clickOnCancelButton() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("cancelButton")));
    }

    public String getNameOfCategoryName() throws IOException {
        return webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("categoryNameFromList"))).getText();
    }

    public void clickOnExactLink(String link) throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("siteOnManageSite").replace("value", link)));
    }

    public List<String> getListOfSiteName() throws IOException {
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("listOfSiteOnManageSite")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("listOfSiteOnManageSite")));
        List<String> siteNameText =  new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public void clickOnGivenIndexSite(int index) throws IOException{
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("listOfSiteOnManageSite")));
        siteName.get(index).click();
    }

    public void clickSiteOption() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("siteOption")));
    }

    public void sendCategoryOnUpdateCat(String catName) throws IOException, InterruptedException {
        WebElement cateName = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("enterCategory")));
        cateName.click();
        cateName.sendKeys(catName);
        cateName.sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        cateName.sendKeys(Keys.ENTER);
        webUtil.hitKey(Keys.ENTER);
    }

    public String verifyUpdateSiteCategory() throws IOException {
        return webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("getCatNameOnSite"))).getText();
    }

    public List<String> verifyPageCategoryOnSite() throws IOException {
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("listOfPageCategoryOnSite")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("listOfPageCategoryOnSite")));
        List<String> siteNameText =  new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public void searchPageCategory(String text) throws IOException {
        WebElement categoryAction = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("searchOnPageCategory")));
        categoryAction.clear();
        categoryAction.sendKeys(text);
        categoryAction.sendKeys(Keys.ENTER);
    }

    public List<String> getListOfFavSites() throws IOException {
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("listOfFavSite")));
        List<WebElement> cardName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("listOfFavSite")));
        List<String> cardNameText =  new LinkedList<>();
        for (WebElement webElement : cardName) {
            cardNameText.add(webElement.getText());
        }
        return cardNameText;
    }

    public int verifyValueFromTheLink(String link) throws IOException {
        String value = ReadProperty.propertyReader("aContains").replace("value", link);
        return webUtil.getElements(By.xpath(value)).size();
    }

    public List<String> getPageCategoryName() throws IOException {
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("getPageCategory")));
        List<WebElement> cardName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("getPageCategory")));
        List<String> cardNameText =  new LinkedList<>();
        for (WebElement webElement : cardName) {
            cardNameText.add(webElement.getText());
        }
        return cardNameText;
    }

    public void clickOnFavourites() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("favoriteOnUserMenu")));
    }

    public void clickOnAddOnPageCategory() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("addCategory")));
    }

    public boolean compareDates() throws IOException, ParseException {
        verifyValueFromTheLink("1");
        String first = webUtil.getElement(By.xpath(ReadProperty.propertyReader("getFirstRowForthColumnValue"))).getText();
        String second = webUtil.getElement(By.xpath(ReadProperty.propertyReader("getSecondRowForthColumnValue"))).getText();
        Date date1=new SimpleDateFormat("MMM dd, yyyy").parse(first);
        Date date2=new SimpleDateFormat("MMM dd, yyyy").parse(second);
        return  date1.after(date2);
    }

    public void selectSortBy(String value) throws IOException {
        webUtil.selectByValue(By.id(ReadProperty.propertyReader("sortBy")), value);
    }

    public boolean verifyDeletePopUP() throws IOException {
        return webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("deleteMessage"))).isDisplayed();
    }

    public List<String> getListOfManegeTopic() throws IOException {
        webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("manageTopicList")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("manageTopicList")));
        List<String> siteNameText =  new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public void sendTopicName(String name) throws IOException {
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("enterTopicName")));
        webUtil.type(By.xpath(ReadProperty.propertyReader("enterTopicName")), "");
        webUtil.type(By.xpath(ReadProperty.propertyReader("enterTopicName")), name);
    }

    public boolean verifyTopicReplace(String title) throws IOException {
        return webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("newTopics").replace("value",title))).isDisplayed();
    }

    public int getTopicReplace(String title) throws IOException {
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader("newTopics").replace("value",title))).size();
    }

    public void searchTopic(String title) throws IOException {
        WebElement categoryAction = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("searchTop")));
        categoryAction.clear();
        categoryAction.sendKeys(title);
        categoryAction.sendKeys(Keys.ENTER);
    }

    public void clickOnDropDownOnManageTopic() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("dropBoxOnTopic")));
    }

    public void clickOnDropDownOnManageSite(String siteName) throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("optionButtonWithNameOnManageSites").replace("value", siteName)));
    }

    public void selectFilter(String value) throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("selectFieldCompact").replace("value", value)));
    }

    public void clickOndropDown() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("fieldCompact")));
    }

    public List<String> getAccessValue() throws IOException {
        webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("getAccess")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("getAccess")));
        List<String> siteNameText =  new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public void clickOnAddButtonOnTopic() throws IOException {
        webUtil.click(By.xpath(ReadProperty.propertyReader("addButtonOnTopic")));
    }

    public int getTheCountOfFavoriteFeedPost() throws IOException {
        webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("listOfFeedPosts")));
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader("listOfFeedPosts"))).size();
    }

    public List<String> getFavoriteContent() throws IOException {
        webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("contentFilter")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("contentFilter")));
        List<String> siteNameText =  new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public List<String> getStampListOnContent() throws IOException {
        webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("stampList")));
        List<WebElement> siteName = webUtil.getElements(By.xpath(ReadProperty.propertyReader("stampList")));
        List<String> siteNameText =  new LinkedList<>();
        for (WebElement webElement : siteName) {
            siteNameText.add(webElement.getText());
        }
        return siteNameText;
    }

    public void sendTopicNameForMerge(String topic) throws IOException, InterruptedException {
        WebElement cateName = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("topicMerge")));
        cateName.click();
        cateName.sendKeys(topic);
        cateName.sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        cateName.sendKeys(Keys.ENTER);
        webUtil.hitKey(Keys.ENTER);
    }


    public void sendPersonName(String topic) throws IOException, InterruptedException {
        WebElement cateName = webUtil.waitForElementToBeVisible(By.xpath(ReadProperty.propertyReader("addPerson")));
        cateName.click();
        cateName.sendKeys(topic);
        cateName.sendKeys(Keys.ENTER);
        Thread.sleep(3000);
        String value = ReadProperty.propertyReader("divContains").replace("1",topic);
        webUtil.clickUsingJavaScript(By.xpath(value));
        webUtil.hitKey(Keys.ENTER);
    }

}

