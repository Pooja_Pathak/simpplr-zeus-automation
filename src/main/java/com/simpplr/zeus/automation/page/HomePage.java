package com.simpplr.zeus.automation.page;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import com.simpplr.zeus.automation.util.ReadProperty;
import com.simpplr.zeus.automation.util.WebUtil;

import com.simpplr.zeus.automation.util.*;



public class HomePage {
    private static final Logger logger = LogManager.getLogger(HomePage.class);

    WebDriver driver;
    WebUtil webUtil;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        webUtil = new WebUtil(driver);
    }

    public void clickOnTab(String tab) throws IOException {
            logger.info("Verify tab : " +tab);
            String value = ReadProperty.propertyReader("divContains").replace("1", tab);
            webUtil.click(By.xpath(value));
    }

    public void clickOnLink(String link) throws IOException {
        String value = ReadProperty.propertyReader("aContains").replace("value", link);
        webUtil.scrollTillElement(webUtil.waitForElementToBeVisible(By.xpath(value)));
        webUtil.click(By.xpath(value));
    }

    public boolean verifyLink(String link) throws IOException {
            String value = ReadProperty.propertyReader("aContains").replace("value", link);
            webUtil.scrollTillElement(webUtil.waitForElementToBeVisible(By.xpath(value)));
            return webUtil.waitForElementToBeVisible(By.xpath(value)).isDisplayed();
    }


	public void clickOnFirstSite() throws IOException{
		String value=ReadProperty.propertyReader("firstSite");
		System.out.println(value);
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("firstSite"))).click();
		//String value=ReadProperty.propertyReader("listOfSite");
		//List<WebElement> listOfsites=webUtil.getElements(By.xpath(value));
		//listOfsites.get(0).click();
		
	}

	public void clickOnAllSite() throws IOException {
		webUtil.getElement(By.xpath(ReadProperty.propertyReader("allSite"))).click();
		
	}

 

    public boolean verifyMessage(String message) throws IOException {
        return webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("divContains").replace("1", message))).isDisplayed();
    }

    public int getMessage(String message) throws IOException {
        webUtil.waitForElementToBePresent(By.xpath(ReadProperty.propertyReader("divContains").replace("1", message)));
        return webUtil.getElements(By.xpath(ReadProperty.propertyReader("divContains").replace("1", message))).size();
    }

}

