package com.simpplr.zeus.automation.page;

import org.openqa.selenium.WebDriver;

import com.simpplr.zeus.automation.base.PageInit;

public class BasePage {

	static WebDriver driver;
	PageInit pageInit;
	
	public AddSitePage addSitePage() {
		return new AddSitePage(driver);
		
	}
	
	public CommonMethods commonMethods() {
		return new CommonMethods(driver);
		
	}
	public LoginPage loginPage() {
		return new LoginPage(driver);
		
	}
	public SitePage sitePage() {
		return new SitePage(driver);
		
	}
	public DashboardPage dashboardPage() {
		return new DashboardPage(driver);
		
	}
	
	public HomePage homePage() {
		return new HomePage(driver);
		
	}

	public ProfileMenuPage profileMenuPage() {
		return new ProfileMenuPage(driver);
	}
	
	public WebDriver getDriverFromPageInit() {
		pageInit=new PageInit();
		try {
			driver = pageInit.initConfiguration();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return driver;
	}

	 public WebDriver getDriver() {
		 return driver;
	 }
	

}
