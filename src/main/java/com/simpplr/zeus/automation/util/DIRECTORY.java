package com.simpplr.zeus.automation.util;

public class DIRECTORY {

	public static final String TEST_RESOURCES_DIR = "src/test/resources";

	public static final String TEST_ENV_PROPERTY_FILE_PATH = "src/main/java/com/simpplr/setup/testEnv.properties";

	public static final String HTML_REPORT_PATH = "HtmlReport/";

	public static final String CONFIG_DIR = TEST_RESOURCES_DIR+ "/config/";

	public static final String CHROMEDRIVER_PATH_WINDOWS = TEST_RESOURCES_DIR
			+ "/selenium-drivers/win/chromedriver.exe";

	public static String FIREFOXDRIVER_PATH_WINDOWS = TEST_RESOURCES_DIR+ "/selenium-drivers/win/geckodriver.exe";
	
	public static String EDGE_PATH_WINDOWS = TEST_RESOURCES_DIR+ "/selenium-drivers/win/msedgedriver.exe";

	public static final String SELENIUM_DRIVERS_FOLDER = "selenium-drivers";

	//public static final String CHROMEDRIVER_PATH_LINUX = CommonUtil.getConfigProperty(TestKeys.CHROME_DRIVER_LINUX);

}
