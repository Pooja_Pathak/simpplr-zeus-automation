package com.simpplr.zeus.automation.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ReadProperty {

	public static Properties readPropertiesFile(String fileName) throws IOException {
	  FileInputStream fis = null;
      Properties prop = null;
      try {
         fis = new FileInputStream(fileName);
         prop = new Properties();
         prop.load(fis);
      } catch(FileNotFoundException fnfe) {
         fnfe.printStackTrace();
      } catch(IOException ioe) {
         ioe.printStackTrace();
      } finally {
         fis.close();
      }
      return prop;
   }
	
	public static String propertyReader(String value) throws IOException {
		FileReader reader=new FileReader("src/test/resources/config/objectRepo.properties");  
		Properties p=new Properties();  
		p.load(reader);
		return p.getProperty(value);
	}
	
}
