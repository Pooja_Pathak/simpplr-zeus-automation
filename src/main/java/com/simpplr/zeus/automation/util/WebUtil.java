package com.simpplr.zeus.automation.util;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WebUtil {

	
	 WebDriver driver;
	
	
	public WebUtil(WebDriver driver) {
		this.driver = driver;
	   
	    

	}
	public void get(String Url) {
		driver.get(Url);
		maximizeBrowser();
	}

	public void maximizeBrowser() {
		driver.manage().window().maximize();
	}

	/**
	 * This function will fetch the element from the application.
	 *
	 * @param locator
	 * @return
	 * @throws Exception
	 */
	public  WebElement getElement(By locator) {
		return waitForElementToBePresent(locator);
	}

	public  WebElement waitForElementToBeVisible(final By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement element = null;
		element = wait.until(ExpectedConditions.visibilityOfElementLocated((By) locator));
		return element;
	}

	
	public  WebElement waitForElementToBeVisible(final By locator, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		WebElement element = null;
		element = wait.until(ExpectedConditions.visibilityOfElementLocated((By) locator));
		return element;
	}

	public  WebElement waitForElementToBeVisible(final String text) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement element = null;
		element = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(),'" + text + "')]")));
		return element;
	}

	public  WebElement waitForElementToBeVisible(final WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(element));
		return element;
	}

	public  WebElement waitForElementToBePresent(final By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement element = null;
		element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		return element;
	}

	public  WebElement waitForElementToBePresent(final By locator, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement element = null;
		element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		return element;
	}

	public  WebElement waitForElementToBePresent(final String text) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement element = null;
		element = wait
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(text(),'" + text + "')]")));
		return element;
	}

	public  boolean isElementVisible(By locator) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			return true;
		} catch (Throwable t) {
			return false;
		}
	}

	public  boolean isElementVisible(By locator, int timeout) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			return true;
		} catch (Throwable t) {
			return false;
		}
	}

	public  boolean isElementPresent(By locator, int timeout) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			return true;
		} catch (Throwable t) {
			return false;
		}
	}

	public  void waitForElementNotPresent(By locator, int waitTime) {
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}

	public  void waitForElementNotPresent(String text, int waitTime) {
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'" + text + "')]")));
	}

	/**
	 * It will type the test data into an input box
	 *
	 * @param locator
	 * @param testData
	 * @throws Exception
	 */
	public  void type(By locator, CharSequence... testData) {
		String logText = "";
		for (int i = 0; i < testData.length; i++) {
			logText = logText + testData[i];
		}
	   WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement element = null;
		element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		element.clear();
		element.sendKeys("");
		element.sendKeys(testData);

	}

	public  void typeUsingActionApi(By locator, String testData) {
		testData = testData.toString().trim();
		Actions actions = new Actions(driver);
		actions.sendKeys(testData).build().perform();
	}
   public  void enterTestDatas(By locator,Object testData) {
	getElement(locator).sendKeys(testData.toString());
	
}
	public  void enterTestData(By locator, Object testData) {
		String value = null;
		boolean isValueVerified = false;
		for (int i = 0; i <= 10; i++) {
			try {
				testData = testData.toString().trim();
				type(locator, testData.toString());
				if (getElement(locator).getAttribute("value") != null) {
					value = getElement(locator).getAttribute("value").trim();
					Assert.assertEquals(value.toLowerCase().trim(), testData.toString().toLowerCase().trim());
					isValueVerified = true;
					break;
				}
			} catch (AssertionError e) {
				waitInSeconds(1);
			}
		}
		if (!isValueVerified) {
			throw new AssertionError("Expected: " + testData + " but found: " + value);
		}
	}

	public  String getInputBoxValue(By locator) {
		return getElement(locator).getAttribute("value").trim();
	}

	/**
	 * It clicks on web element
	 *
	 * @param locator
	 * @throws Exception
	 */
	public  void click(By locator) {
		try {
			waitForElementToBeVisible(locator).click();
		} catch (WebDriverException e) {
			clickUsingJavaScript(locator);
		}
	}

	public  void setImplicitWaitOnDriver(int maxWaitTime) {
		driver.manage().timeouts().implicitlyWait(maxWaitTime, TimeUnit.SECONDS);
	}

	public  void clickUsingJavaScript(By locator) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement element = waitForElementToBePresent(locator);
		js.executeScript("arguments[0].click();", element);
	}

	public  String getText(By locator) {
		try {
			return waitForElementToBePresent(locator).getText().trim();
		} catch (StaleElementReferenceException s) {
			getText(locator);
		}
		return null;
	}

	public  void takeScreenShot(String filePath) throws IOException {
		Augmenter augmenter = new Augmenter();
		TakesScreenshot ts = (TakesScreenshot) augmenter.augment(driver);
		File scrFile = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(filePath));
	}

	public static void waitInSeconds(long seconds) {
		try {
			Thread.sleep(1000 * seconds);
		} catch (InterruptedException e) {
		}
	}

	public  void clickUsingActionApi(By locator) {
		Actions actions = new Actions(driver);
		actions.moveToElement(waitForElementToBeVisible(locator)).click().perform();
	}

	public  List<WebElement> getElements(By locator) {
		return driver.findElements(locator);
	}

	public  void selectByValue(By locator, String testData) {
		WebElement element = waitForElementToBePresent(locator);
		Select s2 = new Select(element);
		s2.selectByValue(testData);

	}

	public  void submitForm(By locator) {
		getElement(locator).submit();
	}

	public  void hitKey(Keys key) {
		Actions builder = new Actions(driver);
		builder.sendKeys(Keys.ENTER).build().perform();
	}

	public  void selectCheckBox(By locator) {
		if (!getElement(locator).isSelected()) {
			getElement(locator).click();
		}
	}

	public  void deSelectCheckBox(By locator) {
		if (getElement(locator).isSelected()) {
			getElement(locator).click();
		}
	}

	public  void refreshBrowser() {
		driver.navigate().refresh();
	}
	
	public  void scrollTillElement(WebElement webElement) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", webElement);
	}
	
	public void scrollVerticallyDown() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,250)", "");
	}
	public void scrollVerticallyUp() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-250)", "");
		
	}

}
