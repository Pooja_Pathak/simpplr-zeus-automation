package StepDefinition;

import com.simpplr.zeus.automation.page.BasePage;
import com.simpplr.zeus.automation.page.CommonMethods;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class ManageSites {
    public static BasePage basePage = new BasePage();
    static String title;
    static String text;
    static int count;

    @And("Click on Avatar from profile menu")
    public void clickOnAvatarFromProfileMenu() throws IOException {
        basePage.loginPage().clickOnAvatar();
    }

    @And("Click on {string} from profile menu")
    public void clickOnFromProfileMenu(String subTab) throws IOException {
        basePage.profileMenuPage().clickOnDivTab(subTab);
    }

    @Then("Verify that Manage sites page")
    public void verifyThatManageSitesPage() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().verifyManageSitePage());
    }

    @And("Verify that after clicking site category UI should show list of site categories")
    public void verifyThatAfterClickingSiteCategoryUIShouldShowListOfSiteCategories() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().getListOfSiteCategory().size()>0);
    }

    @And("Verify that UI should show filter option and by default it is applied as A-Z")
    public void verifyThatUIShouldShowFilterOptionAndByDefaultItIsAppliedAsAZ() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().verifyAZFilter());
    }

    @And("Enter category name and select who can add sites in category")
    public void enterCategoryNameAndSelectWhoCanAddSitesInCategory() throws IOException {
        title = "Category_" + CommonMethods.getRandomString().substring(4).toUpperCase();
        basePage.profileMenuPage().sendCategoryName(title);
    }

    @Then("Verify that from site categories page user should be able to add category")
    public void verifyThatFromSiteCategoriesPageUserShouldBeAbleToAddCategory() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().verifyCategory(title));
    }

    @And("Enter string in search field on site categories")
    public void enterStringInSearchFieldOnSiteCategories() throws IOException {
        title = basePage.profileMenuPage().getListOfSiteCategory().get(1);
        basePage.profileMenuPage().searchCategoryOnSiteCategory(title);
    }

    @And("Search site {string} on my site page")
    public void searchStringInSearchFieldOnSite(String site) throws IOException {
        basePage.profileMenuPage().searchCategoryOnSiteCategory(site);
    }

    @Then("Verify that if site category search by user is not present then UI should throw proper error")
    public void verifyThatIfSiteCategorySearchByUserIsNotPresentThenUIShouldThrowProperError() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().verifyNothingToShow());
    }

    @When("Click on clear search or cross button")
    public void clickOnClearSearchOrCrossButton() throws IOException {
        basePage.profileMenuPage().clickOnClearSearch();
    }

    @And("Verify that pagination is implemented on site category page")
    public void verifyThatPaginationIsImplementedOnSiteCategoryPage() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().getListOfSiteCategory().size()<=16);
    }

    @And("Verify that after clicking on show more button rest of site category list should listed on UI")
    public void verifyThatAfterClickingOnShowMoreButtonRestOfSiteCategoryListShouldListedOnUI() throws IOException {
        int count = basePage.profileMenuPage().getListOfSiteCategory().size();
        int showMore = basePage.sitePage().getTheSpanCount("Show more");
        if(showMore!=0){
            basePage.sitePage().clickOnSpan("Show more");
            int countAfter = basePage.profileMenuPage().getListOfSiteCategory().size();
            Assert.assertTrue(countAfter>16);
        }else{
            Assert.assertTrue(count<16);
        }
    }

    @And("Select {string} site option from options column")
    public void selectSiteOptionFromOptionsColumn(String value) throws IOException {
        basePage.profileMenuPage().clickOnDropFromDeleteAndEdit();
        basePage.profileMenuPage().clickOnDropWithValue(value);
    }

    @When("Edit Category name on edit site category pop up")
    public void editCategoryNameOnEditSiteCategoryPopUp() throws IOException {
        title = "Category_" + CommonMethods.getRandomString();
        text = basePage.profileMenuPage().getCategoryValue();
        basePage.profileMenuPage().sendCategoryName(text);
    }

    @And("Click on save button on site category popup")
    public void clickOnSaveButtonOnSiteCategoryPopup() throws IOException, InterruptedException {
        basePage.profileMenuPage().clickOnSaveButton();
        Thread.sleep(3000);
    }

    @Then("Verify that after selecting edit option popup for edit should display on UI and on saving changes site category should update")
    public void verifyThatAfterSelectingEditOptionPopupForEditShouldDisplayOnUIAndOnSavingChangesSiteCategoryShouldUpdate() throws IOException {
        Assert.assertFalse((basePage.profileMenuPage().getListOfCategoryName()).contains(text));
        Assert.assertTrue((basePage.profileMenuPage().getListOfCategoryName()).contains(title));
    }

    @Then("Verify that after if user want to cancel edit then it should cancel after clicking cancel button")
    public void verifyThatAfterIfUserWantToCancelEditThenItShouldCancelAfterClickingCancelButton() throws IOException {
        Assert.assertTrue((basePage.profileMenuPage().getListOfCategoryName()).contains(text));
        Assert.assertFalse((basePage.profileMenuPage().getListOfCategoryName()).contains(title));
    }

    @And("Click on cancel button on site category popup")
    public void clickOnCancelButtonOnSiteCategoryPopup() throws IOException {
        basePage.profileMenuPage().clickOnCancelButton();
    }

    @And("Get the name of site category")
    public void getTheNameOfSiteCategory() throws IOException {
        title = basePage.profileMenuPage().getNameOfCategoryName();
    }

    @Then("Verify that user should be able to delete site category")
    public void verifyThatUserShouldBeAbleToDeleteSiteCategory() throws IOException, InterruptedException {
        Thread.sleep(4000);
        Assert.assertFalse((basePage.profileMenuPage().getListOfCategoryName()).contains(title));
    }

    @When("Click on link {string}")
    public void clickOnLink(String link) throws IOException {
        basePage.profileMenuPage().clickOnExactLink(link);
    }

    @And("Verify that user should be able to see list of all sites under sites tab")
    public void verifyThatUserShouldBeAbleToSeeListOfAllSitesUnderSitesTab() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().getListOfSiteName().size()<=16, basePage.profileMenuPage().getListOfSiteName().size() +" size of list");
    }

    @And("Verify that user should be able filter all active sites and by default Active filter is applied")
    public void verifyThatUserShouldBeAbleFilterAllActiveSitesAndByDefaultActiveFilterIsApplied() throws IOException {
        Assert.assertEquals(basePage.sitePage().getTheSpanCount("Deactivated"), 0, basePage.sitePage().getTheSpanCount("Deactivated") + " is a size ");
    }

    @When("Enter string in search box and press enter")
    public void enterStringInSearchBoxAndPressEnter() throws IOException {
        title = basePage.profileMenuPage().getListOfSiteName().get(2);
        basePage.profileMenuPage().searchCategoryOnSiteCategory(title);
    }

    @When("Search the site name {string} in site search")
    public void enterStringInSearchBox(String site) throws IOException {
        basePage.profileMenuPage().searchCategoryOnSiteCategory(site);
    }

    @Then("Verify that user should be able to search sites by entering site name")
    public void verifyThatUserShouldBeAbleToSearchSitesByEnteringSiteName() throws IOException {
        Assert.assertEquals(basePage.profileMenuPage().getListOfSiteName().size(), 1);
    }

    @And("Click on site option on site tab")
    public void clickOnSiteOptionOnSiteTab() throws IOException {
        basePage.profileMenuPage().clickSiteOption();
    }

    @And("Select new category from dropdown or enter string {string}")
    public void selectNewCategoryFromDropdownOrEnterString(String value) throws IOException, InterruptedException {
        basePage.profileMenuPage().sendCategoryOnUpdateCat(value);
    }

    @Then("Verify that user should be able to update category as {string} of site from update category option from dropdown of options column")
    public void verifyThatUserShouldBeAbleToUpdateCategoryAs(String value) throws InterruptedException, IOException {
        Thread.sleep(2000);
        Assert.assertEquals(basePage.profileMenuPage().verifyUpdateSiteCategory(),value);
    }

    @And("Click on {int} site")
    public void clickOnSite(int index) throws IOException {
        title = basePage.profileMenuPage().getListOfSiteName().get(index-1);
        basePage.profileMenuPage().clickOnGivenIndexSite(index-1);
    }

    @Then("Verify that user should be able to see list of page categories on page category tab")
    public void verifyThatUserShouldBeAbleToSeeListOfPageCategoriesOnPageCategoryTab() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().verifyPageCategoryOnSite().size()>=1);
    }

    @And("Enter string in search box on page categories")
    public void enterStringInSearchBoxOnPageCategories() throws IOException {
        title = basePage.profileMenuPage().verifyPageCategoryOnSite().get(1);
        basePage.profileMenuPage().searchPageCategory(title);
    }

    @Then("Verify that user should be able to search page category on page category tab")
    public void verifyThatUserShouldBeAbleToSearchPageCategoryOnPageCategoryTab() throws IOException {
        Assert.assertEquals(basePage.profileMenuPage().verifyPageCategoryOnSite().size(),1);
    }

    @Then("Verify that user should be able to view the site which is marked as favorite")
    public void verifyThatUserShouldBeAbleToViewTheSiteWhichIsMarkedAsFavorite() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().getListOfFavSites().size()>0 || basePage.profileMenuPage().getListOfFavSites().size()==0);
    }

    @When("Search the site from search box on favorite site")
    public void searchTheSiteFromSearchBoxOnFavoriteSite() throws IOException {
        title = basePage.profileMenuPage().getListOfFavSites().get(0);
        basePage.profileMenuPage().searchCategoryOnSiteCategory(title);
    }

    @And("Verify that user should be able to search the favorite site from search option")
    public void verifyThatUserShouldBeAbleToSearchTheFavoriteSiteFromSearchOption() throws IOException {
        Assert.assertEquals(basePage.profileMenuPage().getListOfFavSites().size(), 1);
    }

    @And("Click on Favourites from profile menu")
    public void clickOnFavouritesFromProfileMenu() throws IOException {
        basePage.profileMenuPage().clickOnFavourites();
    }

    @And("Enter Category name on page categories")
    public void enterCategoryNameOnPageCategories() throws IOException, InterruptedException {
        text = CommonMethods.getRandomString();
        Thread.sleep(2000);
        basePage.sitePage().typeCategoryName(text);
    }

    @When("Click on add button on page category")
    public void clickOnAddButtonOnPageCategory() throws IOException {
        basePage.profileMenuPage().clickOnAddOnPageCategory();
    }

    @Then("Verify that user should be able to add new page category by clicking on Add category")
    @Then("Verify that user should be able edit Page category")
    @Then("Verify that application should not delete page category if user click cancel on confirm delete popup")
    public void userShouldSeeOptionAtIndex() throws IOException, InterruptedException {
        Thread.sleep(6000);
        Assert.assertEquals(basePage.profileMenuPage().verifyValueFromTheLink(text),1);
    }

    @Then("Verify that user should be able to add new page category by clicking on cancel button")
    @Then("Verify that user should not be able edit Page category if user clicks cancel button")
    public void verifyThatUserShouldBeAbleToAddNewPageCategoryByClickingOnCancelButton() throws IOException {
        Assert.assertEquals(basePage.profileMenuPage().verifyValueFromTheLink(text),0);
    }

    @Then("Verify that user should be able to filter Page category with newly created page category first")
    public void verifyThatUserShouldBeAbleToFilterPageCategoryWithNewlyCreatedPageCategoryFirst() throws IOException, ParseException, InterruptedException {
        Thread.sleep(2000);
        Assert.assertTrue(basePage.profileMenuPage().compareDates());
    }

    @And("select sort by value as {string} on page category")
    public void selectSortByValueAsOnPageCategory(String value) throws IOException {
        basePage.profileMenuPage().selectSortBy(value);
    }

    @Then("Verify that user should be able to filter Page category with oldest created page category first.")
    public void verifyThatUserShouldBeAbleToFilterPageCategoryWithOldestCreatedPageCategoryFirst() throws IOException, ParseException {
        Assert.assertFalse(basePage.profileMenuPage().compareDates());
    }

    @And("Get the name of page Category")
    public void getTheNameOfPageCategory() throws IOException {
        text = basePage.profileMenuPage().getPageCategoryName().get(0);
    }

    @Then("Verify that application should give warning message before removing page category")
    public void verifyThatApplicationShouldGiveWarningMessageBeforeRemovingPageCategory() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().verifyDeletePopUP());
    }

    @Then("Verify that after clicking on members tab all the sites that user is part of should be listed")
    public void verifyThatAfterClickingOnMembersTabAllTheSitesThatUserIsPartOfShouldBeListed() throws IOException {
        Assert.assertTrue(basePage.sitePage().getMembersList().size()>0);
    }

    @Then("Verify that application should show list of topics")
    public void verifyThatApplicationShouldShowListOfTopics() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().getListOfManegeTopic().size()>0);
    }

    @And("Enter topic name into input box")
    public void enterTopicNameIntoInputBox() throws IOException {
        title = CommonMethods.getRandomString().substring(5).toUpperCase();
        basePage.profileMenuPage().sendTopicName(title);
    }

    @Then("Verify that application should allow to add new topics")
    @Then("Verify that application should allow to search topic by keywords")
    public void verifyThatApplicationShouldAllowToAddNewTopics() throws IOException, InterruptedException {
        Thread.sleep(5000);
        Assert.assertTrue(basePage.profileMenuPage().getListOfManegeTopic().contains(title), "Doesn't contains the title "+title);
    }

    @And("Enter Duplicate topic name into input box")
    public void enterDuplicateTopicNameIntoInputBox() throws IOException {
        title = basePage.profileMenuPage().getListOfManegeTopic().get(1);
        basePage.profileMenuPage().sendTopicName(title);
    }

    @And("Search the topic on Manage Topic")
    public void searchTheTopicOnManageTopic() throws IOException {
        title = basePage.profileMenuPage().getListOfManegeTopic().get(1);
        basePage.profileMenuPage().searchTopic(title);
    }

    @Then("Verify that application should allow to replace the original top")
    public void verifyThatApplicationShouldAllowToReplaceTheOriginalTop() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().verifyTopicReplace(title));
    }

    @And("Enter topic name with change in case sensitivity")
    public void enterTopicNameWithChangeInCaseSensitivity() throws IOException {
        title = basePage.profileMenuPage().getListOfManegeTopic().get(1);
        basePage.profileMenuPage().sendTopicName(title.toUpperCase());
    }

    @When("Search the topic {string} on Manage Topic")
    public void searchTheTopicOnManageTopic(String text) throws IOException {
        title= text;
        basePage.profileMenuPage().searchTopic(title);
    }

    @Then("Verify that application should show no result as there is no topic in list which searched keyword")
    public void verifyThatApplicationShouldShowNoResultAsThereIsNoTopicInListWhichSearchedKeyword() throws IOException {
        Assert.assertFalse(basePage.profileMenuPage().getListOfManegeTopic().contains(title), "Does contains the title "+title);
    }

    @And("Click on option menu dropdown")
    public void clickOnOptionMenuDropdown() throws IOException {
        basePage.profileMenuPage().clickOnDropDownOnManageTopic();
    }

    @And("In Filter dropdown select {string}")
    public void inFilterDropdownSelect(String value) throws IOException {
        basePage.profileMenuPage().clickOndropDown();
        basePage.profileMenuPage().selectFilter(value);
    }

    @Then("Verify that user should be able filter all {string} sites")
    public void verifyThatUserShouldBeAbleFilterAllSites(String value) throws IOException {
        List<String> list = basePage.profileMenuPage().getAccessValue();
        for (String s : list) {
            Assert.assertEquals(s, value);
        }
    }

    @Then("Verify that user should be able filter all Deactivated sites")
    public void verifyThatUserShouldBeAbleFilterAllDeactivatedSites() throws IOException {
        List<String> list = basePage.profileMenuPage().getAccessValue();
        int size = basePage.sitePage().getTheSpanCount("Deactivated");
        Assert.assertEquals(list.size(),size);
    }

    @Then("Verify that user should be able filter all sites")
    public void verifyThatUserShouldBeAbleFilterAllSites() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().getAccessValue().contains("Public"));
        Assert.assertTrue(basePage.profileMenuPage().getAccessValue().contains("Private"));
    }

    @Then("Verify that user should be able filter Latest sites")
    public void verifyThatUserShouldBeAbleFilterLatestSites() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().getListOfSiteCategory().contains("Selenium"));
    }

    @When("Click on Add button on Add topics")
    public void clickOnAddButtonOnAddTopics() throws IOException {
        basePage.profileMenuPage().clickOnAddButtonOnTopic();
    }

    @Then("Verify that application should not edit topic  name when user click on cancel button")
    public void verifyThatApplicationShouldNotEditTopicNameWhenUserClickOnCancelButton() throws IOException, InterruptedException {
        Thread.sleep(2000);
        Assert.assertEquals(basePage.profileMenuPage().getTopicReplace(title), 0);
    }

    @Then("Verify that application should not allow duplicate topic")
    public void verifyThatApplicationShouldNotAllowDuplicateTopic() throws IOException {
        Assert.assertTrue(basePage.homePage().verifyMessage("Could not edit Topic - Only character spacing and capitalization allowed"));
    }

    @And("Get the count of topics on manage topic")
    public void getTheCountOfTopicsOnManageTopic() throws IOException {
        count = basePage.profileMenuPage().getListOfManegeTopic().size();
    }

    @Then("Verify that application should allow to delete topic")
    public void verifyThatApplicationShouldAllowToDeleteTopic() throws IOException, InterruptedException {
        Thread.sleep(5000);
        Assert.assertNotEquals(basePage.profileMenuPage().getListOfManegeTopic().size(), count);
    }

    @Then("Verify that application should not delete topic when user click on cancel button")
    public void verifyThatApplicationShouldNotDeleteTopicWhenUserClickOnCancelButton() throws IOException {
        Assert.assertEquals(basePage.profileMenuPage().getListOfManegeTopic().size(), count);
    }

    @Then("Verify that pagination feature is available on topics page")
    public void verifyThatPaginationFeatureIsAvailableOnTopicsPage() throws IOException {
        int count = basePage.profileMenuPage().getListOfManegeTopic().size();
        int showMore = basePage.sitePage().getTheSpanCount("Show more");
        if(showMore!=0){
            basePage.sitePage().clickOnSpan("Show more");
            int countAfter = basePage.profileMenuPage().getListOfManegeTopic().size();
            Assert.assertTrue(countAfter>16);
        }else{
            Assert.assertTrue(count<16);
        }
    }

    @Then("Verify that user should be able to see list of content which are marked as favorites and also should be able to apply filter")
    public void verifyThatUserShouldBeAbleToSeeListOfContentWhichAreMarkedAsFavoritesAndAlsoShouldBeAbleToApplyFilter() throws IOException {
       List<String> content = basePage.profileMenuPage().getFavoriteContent();
       for(String s: content){
           Assert.assertEquals(s,"Page");
       }
    }

    @Then("Verify that user should be able to see list of feed which are marked as favorite")
    public void verifyThatUserShouldBeAbleToSeeListOfFeedWhichAreMarkedAsFavorite() throws IOException {
        Assert.assertTrue(basePage.profileMenuPage().getTheCountOfFavoriteFeedPost()>0);
    }

    @Then("Verify that user should be able to view the content which are in draft or pending state via Manage site")
    public void verifyThatUserShouldBeAbleToViewTheContentWhichAreInDraftPendingStateViaManageSite() throws IOException {
        List<String> stampValue = basePage.profileMenuPage().getStampListOnContent();
        for(String s: stampValue){
            Assert.assertTrue(s.equalsIgnoreCase("Published") ||s.equalsIgnoreCase("draft") || s.equalsIgnoreCase("Pending"));
        }
    }

    @And("Now search for any other topic")
    public void nowSearchForAnyOtherTopic() throws IOException, InterruptedException {
        title = basePage.profileMenuPage().getListOfManegeTopic().get(0);
        String value = basePage.profileMenuPage().getListOfManegeTopic().get(2);
        basePage.profileMenuPage().sendTopicNameForMerge(value);
    }

    @Then("Merge should be done and the topic which was merge should not be there in the topic list")
    public void mergeShouldBeDoneAndTheTopicWhichWasMergeShouldNotBeThereInTheTopicList() throws IOException, InterruptedException {
        Thread.sleep(2000);
        Assert.assertFalse(basePage.profileMenuPage().getListOfManegeTopic().contains(title));
    }
}
