package StepDefinition;

import com.simpplr.zeus.automation.page.BasePage;

import com.simpplr.zeus.automation.page.CommonMethods;import com.simpplr.zeus.automation.util.ReadProperty;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import java.io.IOException;

public class HomeSiteDashboard {
	public static BasePage basePage = new BasePage();
	static String tileName;
	static String text;
	static int count;
	static String status;
    
	
	@And("Click on first available site")
    public void ClickOnFirstSite() throws IOException {
	  basePage.homePage().clickOnAllSite();
	  basePage.homePage().clickOnFirstSite();
     }
	
	
	@And("Select any site own/manage by user")
	 public void selectSite() {
		
	 }
	   @And("Goto Dashboard and feed tab")
	   public void goto_dashB_Feed() {
		   
	   }
	   @Given("Click on dashboard setting option on bottom right corner of screen")
       public void cickDash_setting() throws IOException {
		   basePage.dashboardPage().clickOnSetting();
	   }
	   @And("Select the layout")
	   public void selectLayout() throws IOException {
		   basePage.dashboardPage().selectLayout();
		   
	   }
	   @When("Click Done")
	   public void clickDone() throws IOException {
		   basePage.dashboardPage().clickDone();
	   }
	   @Then("Application layout should change as selected by user without any error")

	   public void displayApplicationlayout() throws Exception {
		   Assert.assertTrue(basePage.dashboardPage().validateFlashMessage("Set dashboard layout successfully"));
	   }
	   
	   
	   @Given("Select favourite icon")
	   public void clickStarIcon() throws IOException {
		   basePage.dashboardPage().SelectStarIcon();
	   }
	   

	   @Given("UnSelect favourite icon")
	   public void UnselectStarIcon() throws IOException {
		   basePage.dashboardPage().UnSelectStarIcon();
	   }
	   
	   
	   @Then("Favourite icon should be selected")
	   public void validateStarIsSelected() {
		   Assert.assertTrue( basePage.dashboardPage().validateStarIsSelected());
	   }
	   
	   @Then("Favourite icon should not be selected")
       public void validateStarIsNotSelected() {
		   Assert.assertFalse( basePage.dashboardPage().validateStarIsSelected());
	   }
	   @When("Select site category tile and enter detail")
	   public void enterSiteCategory() {
		   basePage.dashboardPage().clickSiteCategory();
		   basePage.dashboardPage().enterSiteCategoryTitle("SiteTesting123");
	   }
	   
	   @And("Enter tile name")
	   public void enterTileName() {
		   basePage.dashboardPage().enterSiteCategoryTitle("TileTesting123");
	   }
	   @And("Enter Title Name")
	   public void addContentTitle(){
		   basePage.dashboardPage().enterSiteCategoryTitle("ContentTesting123");
	   }
	   @And("Enter details in Add sites & categories tile")
	   public void enterDetailsinSiteAndCategories() throws IOException {
		   basePage.dashboardPage().enterSiteCategoryDetails("Xaviers");
	   }
	   
	   @And("The added site category should be displayed after refresh")
	   public void validateAddedCategory() throws Exception {
		  // Assert.assertTrue(basePage.dashboardPage().validateSiteCategory("Xaviers"));
		   Assert.assertTrue(basePage.dashboardPage().validateSiteCategoryAdded());
	   }
	   @When("Select site category tab")
	   public void selectSiteCategoryTab() {
		   basePage.dashboardPage().selectSiteCategoryTab();
	   }
	   
	   @Given("There is no carousel on Dashboard Application mentions empty carousel")
	   public void there_is_no_carousel_on_Dashboard_Application_mentions_empty_carousel() throws IOException {
	      Assert.assertTrue(basePage.dashboardPage().validateEmptyCarousel());
	   }

	   @Given("Click on Manage caraousel")
	   public void click_on_Manage_caraousel() throws IOException {
	     basePage.dashboardPage().clickManageCaroUsel();
	   }

	   @Given("Enter Search content {string}")
	   public void enter_Serach_content(String searchContent) throws IOException, InterruptedException {
		    basePage.sitePage().sendOnSearchCarousel(searchContent);
	       //basePage.dashboardPage().enterSearchContent(searchContent);
	   }

	   @Given("Click on Add carousel")
	   public void click_on_Add_carousel() {
		   basePage.dashboardPage().clickDoneCarousel();
	   }

	   @Given("Click on options on carousel")
	   public void click_on_options_on_carousel() throws Exception {
		   Thread.sleep(2000);
	       basePage.dashboardPage().clickOnCarouselOptions();
	   }

	   @Given("Click on Start-Autoplay")
	   public void click_on_Start_Autoplay() throws IOException {
		   basePage.dashboardPage().clickOnStartAutoPlay();
	   }

	   @Given("Click on Stop-Autoplay")
	   public void click_on_Stop_Autoplay() throws IOException {
		   basePage.dashboardPage().clickOnStopAutoPlay();
	   }

	   @When("Select edit carousel")
	   public void select_edit_carousel() throws IOException {
		   basePage.dashboardPage().clickEditCarousel();
	   }

	   @When("Select content and move up and down")
	   public void select_content_and_move_up_and_down() throws IOException {
		   basePage.dashboardPage().dragCarouselContent();
	       
	   }

	  

	   @When("Click to remove all carousel content")
	   public void click_on_cross_button_of_content_which_is_tobe_removed_from_list() throws Exception {
	       
		   basePage.dashboardPage().removeAllCaroselContent();
	   }

	  
	   
	
	   
	   @Given("Click on Site Category Option")
	   public void clickOnSiteCategoryOption() throws IOException {
		   
		   basePage.dashboardPage().clickSitecategoryOptions();
	   }
	   

	   @Given("Click on Content Option")
	   public void clickOnContentOption() throws Exception {
		   Thread.sleep(2000);
		   basePage.dashboardPage().clickOnContentOptions();
	   }

	 
	    @When("Click on {string} option")
	    public void clickOnOption(String option) {
	    	
	    	basePage.dashboardPage().clickSitecategoryOptionER(option);
	    }
	   
	    @And("Click on Save")
	    public void clickOnSave() {
	    	basePage.dashboardPage().clickOnEditSave();
	    }
	    @Then("Application should remove tile from dashboard")
	    public void validateSiteCategoryRemoved() throws IOException {
	    	
	    	Assert.assertTrue(basePage.dashboardPage().validateFlashMessage("Removed tile from dashboard successfully"));
	    	
	    }
	    
	    @Then("Application should edit tile successfully")
      public void validateEditMessage() throws IOException {
	    	
	    	Assert.assertTrue(basePage.dashboardPage().validateFlashMessage("Edited dashboard tile successfully"));
	    	
	    }
	    
	    @Then("Application should edit site category tile from dashboard")
	    public void validateSiteCategoryEdited() {
	    }
	   
	   @And("Select latest & popular tab and fill all the information")
	   public void selectlatestTab() {
		   
	   }
	   
	  
	   @Then("Application should allow user to add latest & popular content on site dashboards")
	   public void contentAddedValidation() {
		   
	   }
	   
	   @Given("Click on Add tile option on bottom right corner of screen")
	   public void clickAddTitleBottomCorner() throws IOException {
		   basePage.dashboardPage().clickOnAddTile();
	   }
	   
	   @And("Click Add to home button")
	   public void clickHomeButton() throws InterruptedException {
		   
		   basePage.dashboardPage().clickOnHomeButton();
	   }
	   @And("Select the tile and fill details")
	   public void fillDetails() throws Exception {
		   basePage.dashboardPage().clickOnAddPageEventsBlogPost();
	   }
	   @When("Click Add to site dashboard")
	   public void clickAddToSite() throws IOException{
		   basePage.dashboardPage().clickOnAddtoSiteDashboard();
		  
	   }
	   @Then("Application should allow user to add tiles")
	   public void validateTileAdded() throws Exception {
		   Assert.assertTrue(basePage.dashboardPage().validateFlashMessage("Added tile to dashboard successfully"));
	   }
	   
	 
	   
	   @Then("Application should allow user to delete content in the dashboard")
	   public void validateContentDeleted() throws IOException {
		   Assert.assertTrue(basePage.dashboardPage().validateDeletedContent());
	   }
	   
	   @Given("UI should allow the drag and drop of a Tile")
	   public void dragdropTile() throws IOException {
		   basePage.dashboardPage().dragdropTile();
	   }
	  
	    
	   
	   @Given("Click on options on tile")
	   public void clickOptionOnTile() throws Exception {
		   basePage.dashboardPage().clickOnOptionsOnTile();
	   }
	   @When("Click on remove")
	   public void clickRemove() throws IOException {
		   basePage.dashboardPage().clickOnRemoveOption();
	   }
	   @And("Again click on remove on warning popup")
	   public void clickRemoveOnWarning() throws IOException {
		   basePage.dashboardPage().clickOnRemovePopup();
	   }
	   
	   @When("Select page category tile in sites section in a popup")
	   public void selectPageCategoryTile() {
		   
		   basePage.dashboardPage().selectPagesCategory();
	   }
	   
	   @And("Select category under Page Category")
	   public void enterDetails() {
		   basePage.dashboardPage().selectcategoryUnderPagecategory();
	   }
	  
	   
	   
	   @And("Select Pages from category tab and fill all the information")
	   public void fillInfoOnPages() throws IOException {
		   
		   basePage.dashboardPage().selectPagesContent();
	   }


    @Then("Verify that user should be able to see add tile and manage dashboard button on homepage")
    @Then("Verify that application should show add and setting button image as logged user has application manager or site manager")
    public void verifyThatUserShouldBeAbleToSeeAddTileAndManageDashboardButtonOnHomepage() throws IOException {
		Assert.assertTrue(basePage.dashboardPage().verifyAddTileIconOnHomePage());
		Assert.assertTrue(basePage.dashboardPage().verifyManageDashboardIcon());
    }

	@Then("Verify that user should have options to add tile on homepage with popup")
	public void verifyThatUserShouldHaveOptionsToAddTileOnHomepageWithPopup() throws IOException {
		Assert.assertTrue(basePage.dashboardPage().verifyAddTilePopUp());
	}

	@When("Click on add tile")
	public void clickOnAddTile() throws IOException, InterruptedException {
		basePage.dashboardPage().clickOnAddTile();
		Thread.sleep(3000);
	}

	@And("Verify that when user add tile on home dashboard there should be option to add content to all user home on add tile popup")
	public void verifyThatWhenUserAddTileOnHomeDashboardThereShouldBeOptionToAddContentToAllUserHomeOnAddTilePopup() throws IOException {
		Assert.assertTrue(basePage.dashboardPage().verifyContentButton());
	}

	@And("Click on {string} paragraph")
	public void clickOnParagraph(String para) throws IOException {
		basePage.dashboardPage().clickOnP(para);
	}

	@And("Enter information on Add tiles")
	public void enterInformationOnAddTiles() throws IOException {
		tileName = CommonMethods.getRandomString();
		basePage.dashboardPage().clickOnTileTitle(tileName);
	}

	@Then("Verify that user should be able to add content on homepage")
	public void verifyThatUserShouldBeAbleToAddContentOnHomepage() throws IOException {
		Assert.assertTrue(basePage.dashboardPage().verifyCreatedTile(tileName));
	}

	@Given("Check Feed tab is enabled")
	public void checkFeedTabIsEnabled() throws IOException, InterruptedException {
		basePage.dashboardPage().enableFeed();
	}

	@Then("Verify that user should be able to add global feed post as favourite")
	public void verifyThatUserShouldBeAbleToAddGlobalFeedPostAsFavourite() throws IOException {
		Assert.assertNotEquals(basePage.dashboardPage().getCountOfFavorite(), count);
	}

	@When("Click on favourite icon next to options on feed post")
	public void clickOnFavouriteIconNextToOptionsOnFeedPost() throws IOException {
		count = basePage.dashboardPage().getCountOfFavorite();
		basePage.dashboardPage().clickOnFavorite();
	}

	@When("Click on like button on post")
	public void clickOnLikeButtonOnPostAndReplies() throws IOException {
		status = basePage.dashboardPage().clickOnLikeButton("1");
	}

	@Then("Verify that user should be able to like the post on global feed post")
	public void verifyThatUserShouldBeAbleToLikeBothPostAndItSRepliesOnGlobalFeedPost() throws IOException {
		basePage.dashboardPage().verifyLikeDislike(status);
	}

	@Then("Verify that user should be able to create replies for index {string} on global feed post which are accessible to user")
	public void verifyThatUserShouldBeAbleToCreateRepliesForIndexOnGlobalFeedPostWhichAreAccessibleToUser(String index) throws IOException {
		basePage.dashboardPage().verifyReplyEditor(index);
	}

	@When("Create a post and send it to the editor")
	public void createAPostAndSendItToTheEditor() throws IOException {
		text = "Hello " + CommonMethods.getRandomString().toUpperCase() + "Feed post";
		basePage.dashboardPage().sendReplyEditor(text, "1");
	}

	@Then("Verify that user should be able to create a global post")
	public void verifyThatUserShouldBeAbleToCreateAGlobalPost() throws IOException {
		basePage.dashboardPage().verifyFeedPosted(text);
	}
}
