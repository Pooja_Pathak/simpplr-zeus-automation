package StepDefinition;

import com.simpplr.zeus.automation.page.BasePage;
import com.simpplr.zeus.automation.page.CommonMethods;
import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class ApplicationManagerSites {

    public static BasePage basePage = new BasePage();
    static String path = System.getProperty("user.dir") + "\\src\\test\\resources\\testData\\";
    static String siteName;
    static String status;
    static String nameTitle;

    @Then("^User should landed on Sites page$")
    public void userShouldLandedOnSitesPage() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyOnHeaderOne("Sites"));
    }
   

    @And("^Site Dashboard page should show list of categories$")
    public void siteDashboardPageShouldShowListOfCategories() throws IOException {
        Assert.assertTrue(basePage.sitePage().VerifyCategoryList() > 0);
    }

    @Then("^Add site form should be displayed$")
    public void addSiteFormShouldBeDisplayed() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyOnHeaderOne("Add site"));
    }

    @And("^Enter all details of site with category \"([^\"]*)\" and access or content \"([^\"]*)\"$")
    public void enterAllDetailsOfSiteWithCategoryAndAccessOrContent(String category, String access) throws IOException {
        siteName = CommonMethods.getRandomString().substring(4).toUpperCase() + "_Site_Name";
        basePage.sitePage().sendSiteName(siteName);
        basePage.sitePage().sendCategoriesName(category);
        basePage.sitePage().selectAccessOrContent(access);
    }

    @Then("^Site should be saved successfully and newly created site populated on the site page$")
    @Then("^Site should be saved successfully with uncategorized category$")
    @Then("^Application should redirect to site without any error$")
    @Then("^Dashboard page should show the site name and category as selected by owner or user with site management permission$")
    public void siteShouldBeSavedSuccessfullyAndNewlyCreatedSitePopulatedOnTheSitePage() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyOnHeaderOne(siteName));
    }

    @And("Enter all details of site with random category and access or content {string}")
    public void enterAllDetailsOfSiteWithRandomCategoryAndAccessOrContent(String access) throws IOException {
        siteName = CommonMethods.getRandomString().toUpperCase() +"_Site_Name";
        basePage.sitePage().sendSiteName(siteName);
        basePage.sitePage().sendCategoriesName(CommonMethods.getRandomString().substring(6).toUpperCase());
        basePage.sitePage().selectAccessOrContent(access);
    }

    @Then("^Site should not be created after cancel site details$")
    public void site_should_not_be_created() {
        Assert.assertEquals("You have unsaved changes. Do you want to leave?", basePage.sitePage().verifyCancelPopUp());
    }

    @Given("Click on {string} site link")
    public void clickOnSiteLink(String index) throws IOException {
        siteName = basePage.sitePage().getTheSiteNameWithIndex(index);
        basePage.sitePage().clickOnSiteLinkWithIndex(index);
    }

    @And("Click on Upload Image on cover and browse image from the system")
    public void clickOnUploadImageOnCoverAndBrowseImageFromTheSystem() throws IOException, AWTException, InterruptedException {
        Thread.sleep(2000);
        basePage.sitePage().uploadCoverImage(path + "image1.jpg");
    }

    @Then("the image should be added as a background image on top of site dashboard")
    public void theImageShouldBeAddedAsABackgroundImageOnTopOfSiteDashboard() throws IOException, InterruptedException {
        Thread.sleep(2000);
        Assert.assertEquals(basePage.sitePage().verifyOnCoverUploadImage(),1);
    }

    @When("Click on {string} link")
    public void clickOnLinkWithIndex(String link) throws IOException {
        basePage.homePage().clickOnLink(link);
    }

    @When("Click on {string} site")
    public void clickSiteWithIndex(String link) throws IOException {
        siteName=link;
        basePage.homePage().clickOnLink(link);
    }

    @When("Click on {string} page")
    public void clickOnPage(String index) throws IOException {
        basePage.sitePage().selectAnyPage(index);
    }

    @And("Click on like or unlike button at bottom of page")
    public void clickOnLikeOrUnlikeButtonAtBottomOfPage() throws IOException {
        status = basePage.sitePage().checkLikeOrDislike();
        basePage.sitePage().clickOnLikeDislikeButton();
    }

    @Then("Verify the content page like dislike status")
    public void verifyTheContentPageLikeDislikeStatus() throws IOException {
        Assert.assertNotEquals(status, basePage.sitePage().checkLikeOrDislike());
    }

    @And("Click on setting option")
    public void clickOnSettingOption() throws IOException {
        basePage.sitePage().clickOnManageDashboard();
    }

    @Then("Verify that there should be a option to add carousel on UI")
    public void verifyThatThereShouldBeAOptionToAddCarouselOnUI() throws IOException {
        Assert.assertTrue(basePage.sitePage().VerifyCarouselPopUp());
    }

    @When("Click on button {string}")
    public void clickOnButtonIndex(String button) throws IOException {
        basePage.sitePage().clickOnButton(button);
    }

    @When("Click on button {string} and wait for sometime")
    public void clickOnButtonAndWait(String button) throws IOException, InterruptedException {
        basePage.sitePage().clickOnButton(button);
        Thread.sleep(3000);
    }

    @And("Search for content in search field")
    public void searchForContentInSearchField() throws IOException, InterruptedException {
        siteName = basePage.sitePage().getTextForFirstCarousel();
        basePage.sitePage().cancelFirstCarousel();
        basePage.sitePage().sendOnSearchCarousel(siteName);

    }

    @Then("Verify that user should be able to search the content to add it in the carousel")
    public void verifyThatUserShouldBeAbleToSearchTheContentToAddItInTheCarousel() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyCarouselChanges(siteName));
    }

    @Then("User should see {string} option")
    public void userShouldSeeOptionAtIndex(String value) throws IOException {
        Assert.assertTrue(basePage.homePage().verifyLink(value));
    }

    @When("Select layout {string} from carousel tab")
    public void selectLayoutFromCarouselTab(String type) throws IOException {
        basePage.sitePage().selectCarouselType(type);
    }

    @Then("Verify that user should have option to select carousel layouts as {string}")
    public void verifyThatUserShouldHaveOptionToSelectCarouselLayoutsAs(String type) throws IOException {
        Assert.assertTrue(basePage.sitePage().verifySelectedCarouselLayout(type));
    }

    @When("Click on {string} button")
    public void clickOnButton(String button) throws IOException {
        basePage.sitePage().clickOnSpan(button);
    }

    @When("Click on Add Content icon")
    public void clickOnAddContentIcon() throws IOException {
        basePage.sitePage().clickOnAddContentOnSite();
    }

    @Then("Verify that application should pop up for Add content site with label {string} ,{string}, {string}")
    public void verifyThatApplicationShouldPopUpForAddContentSiteWithLabel(String label1, String label2, String label3) throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyAddSiteLabels(label1));
        Assert.assertTrue(basePage.sitePage().verifyAddSiteLabels(label2));
        Assert.assertTrue(basePage.sitePage().verifyAddSiteLabels(label3));
    }

    @Then("Verify that user should be logged in successfully aa App Manager")
    public void verifyThatUserShouldBeLoggedInSuccessfullyAaAppManager() throws IOException {
        basePage.loginPage().clickOnAvatar();
        int countTenantManager =  basePage.loginPage().getManageTenantCountUnderProfileMenu();
        Assert.assertEquals(1, countTenantManager);
    }

    @Then("Verify that user should be logged in successfully aa End User")
    public void verifyThatUserShouldBeLoggedInSuccessfullyAaEndUser() throws IOException {
        basePage.loginPage().clickOnAvatar();
        int countTenantManager =  basePage.loginPage().getManageTenantCountUnderProfileMenu();
        Assert.assertEquals(0, countTenantManager);
    }

    @When("Click on Add button")
    public void clickOnAddButton() throws IOException {
        basePage.sitePage().clickOnAdd();
    }

    @Then("Verify that application should add page to the selected site")
    public void verifyThatApplicationShouldAddPageToTheSelectedSite() throws IOException, InterruptedException {
        Thread.sleep(2000);
        Assert.assertTrue(basePage.sitePage().verifyAddPage(siteName));
    }

    @And("select the Add content label as {string}")
    public void selectTheAddContentLabelAs(String text) throws IOException {
        basePage.sitePage().clickAddSiteLabels(text);
    }

    @And("Select site from the drop down {string}")
    public void selectSiteFromTheDropDown(String site) throws IOException {
        siteName = site;
        basePage.sitePage().chooseSite(site);
    }

    @And("Enter all details for creating page with category {string} and access or content {string}")
    public void enterAllDetailsForCreatingPageWithCategoryAndAccessOrContent(String category, String content) throws IOException {
        nameTitle = CommonMethods.getRandomString().toUpperCase() + "_Page_Title";
        basePage.sitePage().sendContentTitle("Page",nameTitle);
        basePage.sitePage().selectContentType(content);
        basePage.sitePage().sendCategoriesOnPage(category);
        basePage.sitePage().sendAddContent(nameTitle + "Added");
    }

    @And("Click on {string} label")
    @And("Select Promote Page As {string}")
    public void clickOnLabel(String label) throws IOException {
        basePage.sitePage().clickOnLabel(label);
    }

    @And("Site Page should show {string} button on the right corner of the page")
    @And("Site Category should show {string} button on the right corner of the page")
    @Then("Verify {string} button on add category pop up")
    public void sitePageShouldShowButtonOnTheRightCornerOfThePage(String title) throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyOnSpan(title));
    }

    @Then("Verify that application show Submit for approval button when logged user created content page")
    @Then("Verify that user should be able to publish the created Add Content")
    public void verifyThatApplicationShowSubmitForApprovalButtonWhenLoggedUserCreatedContentPage() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyOnHeaderOne(nameTitle));
    }

    @When("Select the file User want to attach {string}")
    public void selectTheFileUserWantToAttach(String image) throws AWTException {
        basePage.sitePage().attachFile(path + image);
    }

    @Then("Verify that user should be able to upload any file properly")
    public void verifyThatUserShouldBeAbleToUploadAnyFileProperly() throws IOException {
            Assert.assertTrue(basePage.sitePage().verifyUploadedImageOnPage());
    }

    @And("Enter all details for creating event with location {string}")
    public void enterAllDetailsForCreatingEventWithLocation(String location) throws IOException {
        nameTitle = CommonMethods.getRandomString().toUpperCase() + "_event_Title";
        basePage.sitePage().sendContentTitle("Event",nameTitle);
        basePage.sitePage().sendLocation(location);
        basePage.sitePage().sendAddContent(nameTitle + "Added");
    }

    @And("Enter all details for creating album")
    public void enterAllDetailsForCreatingAlbum() throws IOException, InterruptedException {
        nameTitle = CommonMethods.getRandomString().toUpperCase() + "_album_Title";
        basePage.sitePage().sendContentTitle("Album",nameTitle);
        basePage.sitePage().sendAddContent(nameTitle + "Added");
        Thread.sleep(3000);
    }

    @Then("Application should view uploaded the site image and site cover image uploaded")
    public void applicationShouldViewUploadedTheSiteImageAndSiteCoverImageUploaded() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyDisplayImage());
    }

    @And("Click on Upload Image button and browse image from the system")
    public void clickOnUploadImageButtonAndBrowseImageFromTheSystem() throws IOException, InterruptedException, AWTException {
        Thread.sleep(5000);
        basePage.sitePage().uploadDisplayImage(path + "image3.jpg");
    }

    @Then("Image should be added as a background image on top of site dashboard")
    public void imageShouldBeAddedAsABackgroundImageOnTopOfSiteDashboard() throws IOException, InterruptedException {
        Thread.sleep(2000);
        Assert.assertEquals(basePage.sitePage().getTheUploadImageCount(),1);
    }

    @Then("Application should show {string} tab is active")
    public void applicationShouldShowTabIsActive(String tab) throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyTabActivated(tab));
    }

    @And("Verify that application should show list of site as per the site categories")
    public void verifyThatApplicationShouldShowListOfSiteAsPerTheSiteCategories() throws IOException {
        Assert.assertTrue(basePage.sitePage().getTheCountOfCategoriesOnCategoryPage()>0);
    }

    @Then("Application should show list of sites under My Sites tab")
    public void applicationShouldShowListOfSitesUnderMySitesTab() throws IOException {
        Assert.assertTrue(basePage.sitePage().getTheCountOfMySiteList()>0);
    }

    @Then("Application should show list of sites under Latest tab")
    public void applicationShouldShowListOfSitesUnderLatestTab() throws IOException {
        Assert.assertTrue(basePage.sitePage().getTheCountOwnerOnSite()>0);
    }

    @And("Verify that application should show list of my sites")
    public void verifyThatApplicationShouldShowListOfMySites() throws IOException {
        Assert.assertTrue(basePage.sitePage().getListOfSiteOnLatestPage().size()>0);
    }

    @Then("Verify the cancel button functionality for the site image upload")
    public void verifyTheCancelButtonFunctionalityForTheSiteImageUpload() throws IOException {
        Assert.assertTrue(basePage.sitePage().checkImageRemoved());
    }

    @When("Click on View All link under categories section")
    public void clickOnViewAllLinkUnderCategoriesSection() throws IOException {
        basePage.sitePage().clickOnCategoryViewAll();
    }

    @Then("Application should redirected to categories page and shows list of all categories and the count of sites linked with the category")
    public void applicationShouldRedirectedToCategoriesPage() throws IOException, InterruptedException {
        Thread.sleep(3000);
        Assert.assertTrue(basePage.sitePage().VerifyCategoryListOnCategoryPage()>0);
    }

    @Given("Get existing site name from recent visited sites at index {string}")
    public void getExistingSiteNameFromRecentVisitedSitesAtIndex(String index) throws IOException {
        siteName = basePage.sitePage().getTheSiteNameWithIndex(index);
    }

    @And("Enter all details of site with category {string} and access or content {string} and duplicate site name")
    public void enterAllDetailsOfSiteWithDuplicateSiteName(String category, String access) throws IOException {
        basePage.sitePage().sendSiteName(siteName);
        basePage.sitePage().sendCategoriesName(category);
        basePage.sitePage().selectAccessOrContent(access);
    }

    @Then("Application should throw error for duplicate site name")
    public void applicationShouldThrowErrorForDuplicateSiteName() throws IOException {
        Assert.assertEquals("Duplicate site name", basePage.sitePage().VerifyDuplicateSite());
    }

    @Then("Application should list of featured sites")
    public void applicationShouldListOfFeaturedSites() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyFeaturedPage());
    }

    @And("Application should show {int} recently visited sites")
    public void applicationShouldShowRecentlyVisitedSites(int count) throws IOException, InterruptedException {
        Assert.assertEquals(count, basePage.sitePage().recentlyVisitedSitesCount().size());
    }

    @When("Enter name of the site from the recently visited site and select from dropdown")
    public void enterNameOfTheSiteFromTheRecentlyVisitedSiteAndSelectFromDropdown() throws IOException, InterruptedException {
        List<String> siteNameText = basePage.sitePage().getListOfSiteOnAllSitePage();
        List<String> featureCardText = basePage.sitePage().getListOfFeatureCardName();
        for (String s : siteNameText) {
            if (!featureCardText.contains(s)) {
                siteName = s;
                break;
            }
        }
        basePage.sitePage().sendFeatureSiteNameAndSelect(siteName);
    }

    @Then("Application should add site a as featured site and populated in the featured site category")
    public void applicationShouldAddSiteAAsFeaturedSiteAndPopulatedInTheFeaturedSiteCategory() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyFeaturedCardName(siteName));
    }

    @And("Application should  show Featured cards with site name, site logo, category")
    public void applicationShouldShowFeaturedCardsWithSiteNameSiteLogoCategory() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyFeaturedCardName(siteName));
        Assert.assertTrue(basePage.sitePage().verifyFeaturedCareCategory(siteName));
        Assert.assertTrue(basePage.sitePage().verifyFeaturedSiteMember(siteName));
        Assert.assertTrue(basePage.sitePage().verifyFeaturedCareOwnerOrFollowing(siteName));
    }

    @When("Enter name of the random site and select from dropdown")
    public void enterNameOfTheRandomSiteAndSelectFromDropdown() throws IOException, InterruptedException {
        nameTitle = CommonMethods.getRandomString();
        basePage.sitePage().sendFeatureSiteName(nameTitle);
    }

    @Then("Application should not show site name in the dropdown list as it is not existing")
    public void applicationShouldNotShowSiteNameInTheDropdownListAsItIsNotExisting() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyInvalidFeaturedSite());
    }

    @Then("Verify that application should not show list of site")
    public void verifyThatApplicationShouldNotShowListOfSite() throws IOException {
        Assert.assertEquals(basePage.sitePage().getTheCountOfMySiteList(),0);
    }

    @Then("Application should not show list of site under Latest tab")
    public void applicationShouldNotShowListOfSiteUnderLatestTab() throws IOException {
        Assert.assertEquals(basePage.sitePage().getListOfSiteOnLatestPage().size(),0);
    }

    @When("Click on Featured card photo or Name")
    public void clickOnFeaturedCardPhotoOrName() throws IOException {
        basePage.sitePage().clickOnAnySiteFromRecentlyVisitedOnFeatured(siteName);
    }

    @When("Click on Featured card categories link")
    public void clickOnFeaturedCardCategoriesLink() throws IOException {
        basePage.sitePage().clickOnCategoryOnFeatureCard();
    }

    @When("Click on Featured card Members link")
    public void clickOnFeaturedCardMembersLink() throws IOException {
        basePage.sitePage().clickOnMembersOnFeature();
    }

    @Then("Application should redirect to members list without any error on site page")
    public void applicationShouldRedirectToMembersListWithoutAnyErrorOnSitePage() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyAboutOnSite());
    }

    @And("Application should not list sites not followed by user")
    public void applicationShouldNotListSitesNotFollowedByUser() throws IOException {
        Assert.assertEquals(basePage.sitePage().verifyFollowButton(),0);
    }

    @And("Application should show followed sites with name, category and members and following button")
    public void applicationShouldShowFollowedSitesWithNameCategoryAndMembersAndFollowingButton() throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyOnSpan("Following"));
    }

    @Then("Application should allow user to unfollow site")
    public void applicationShouldAllowUserToUnfollowSite() throws IOException {
        Assert.assertEquals(basePage.sitePage().verifyFollowButton(),1);
        //cleanup
        basePage.sitePage().clickOnSpan("Follow");
    }

    @Then("Application should have pagination and after clicking show more it should show rest of sites as user scroll down")
    public void applicationShouldHavePaginationAndAfterClickingShowMoreItShouldShowRestOfSitesAsUserScrollDown() throws IOException {
        int count = basePage.sitePage().getTheCountOfEntryOnPage();
        int showMore = basePage.sitePage().getTheSpanCount("Show more");
        if(showMore!=0){
            basePage.sitePage().clickOnSpan("Show more");
            int countAfter = basePage.sitePage().getTheCountOfEntryOnPage();
            Assert.assertTrue(countAfter>16);
        }else{
            Assert.assertTrue(count<16);
        }
    }

    @When("Enter the string user want to search")
    public void enterTheStringUserWantToSearch() throws IOException {
        siteName = basePage.sitePage().getListOfSiteOnLatestPage().get(1);
        basePage.sitePage().searchSiteOnFollowingTab(siteName);
    }

    @Then("Application should allow user to search sites")
    public void applicationShouldAllowUserToSearchSites() throws IOException {
        Assert.assertEquals(siteName, basePage.sitePage().getListOfSiteOnLatestPage().get(1));
    }

    @When("Enter the not followed string user want to search")
    public void enterTheNotFollowedStringUserWantToSearch() throws IOException {
        basePage.sitePage().searchSiteOnFollowingTab("wrongStatement");
    }

    @Then("Application should throw proper error")
    public void applicationShouldThrowProperError() throws IOException {
        Assert.assertEquals(0, basePage.sitePage().getListOfSiteOnLatestPage().size());
    }

    @Then("Application should show list of popular sites")
    public void applicationShouldShowListOfPopularSites() throws IOException, InterruptedException {
        Thread.sleep(2000);
        Assert.assertTrue(basePage.sitePage().getListOfSiteOnLatestPage().size()>0);
    }

    @And("Application should show most visited {string}")
    public void applicationShouldShowMostVisited(String popular) throws IOException {
        Assert.assertTrue(basePage.sitePage().getListOfSiteOnLatestPage().contains(popular));
    }

    @And("Verify that pagination is implemented on members tab.")
    public void verifyThatPaginationIsImplementedOnMembersTab() throws IOException {
        int count = basePage.sitePage().getMembersList().size();
        int showMore = basePage.sitePage().getTheSpanCount("Show more");
        if(showMore!=0){
            basePage.sitePage().clickOnSpan("Show more");
            int countAfter = basePage.sitePage().getMembersList().size();
            Assert.assertTrue(countAfter>16);
        }else{
            Assert.assertTrue(count<16);
        }
    }

    @And("Verify that pagination is implemented on page category tab")
    public void verifyThatPaginationIsImplementedOnPageCategoryTab() throws IOException {
        int count = basePage.profileMenuPage().verifyPageCategoryOnSite().size();
        int showMore = basePage.sitePage().getTheSpanCount("Show more");
        if(showMore!=0){
            basePage.sitePage().clickOnSpan("Show more");
            int countAfter = basePage.profileMenuPage().verifyPageCategoryOnSite().size();
            Assert.assertTrue(countAfter>16);
        }else{
            Assert.assertTrue(count<16);
        }
    }

    @And("Verify that application should show recently visited sites {string} on Featured site")
    public void verifyThatApplicationShouldShowRecentlyVisitedSitesOnFeaturedSite(String value) throws IOException, InterruptedException {
        Assert.assertTrue(basePage.sitePage().recentlyVisitedSitesCount().contains(value));
    }

    @And("Verify that application should not show recently visited sites {string} on Featured site")
    public void verifyThatApplicationShouldNotShowRecentlyVisitedSitesOnFeaturedSite(String value) throws IOException, InterruptedException {
        Assert.assertFalse(basePage.sitePage().recentlyVisitedSitesCount().contains(value));
    }

    @And("Verify that application should not show {string} as Featured site")
    public void verifyThatApplicationShouldNotShowAsFeaturedSite(String value) throws IOException {
        Assert.assertFalse(basePage.sitePage().getFeaturedList().contains(value));
    }

    @When("Click on cancel button on add site pop up")
    public void clickOnCancelButtonOnAddSitePopUp() throws IOException {
        basePage.sitePage().clickOnCancelButton();
    }

    @And("Click on {string} tab")
    public void clickOnTab(String tab) throws IOException {
        basePage.homePage().clickOnTab(tab);
    }

    @Then("Verify that user should be able to drag and drop cards on these page")
    public void verifyThatUserShouldBeAbleToDragAndDropCardsOnThesePage() throws IOException {
        List<String> abc= basePage.sitePage().getListOfFeatureCardName();
        basePage.sitePage().dragAndDropFeaturedCard();
        List<String> xyz= basePage.sitePage().getListOfFeatureCardName();
        Assert.assertNotEquals(xyz.get(0),abc.get(0));
    }

    @Then("Verify that for public sites there is option make site {string}")
    public void verifyThatForPublicSitesThereIsOptionMakeSite(String access) throws IOException {
        Assert.assertTrue(basePage.sitePage().verifySiteAccess(siteName,access));
    }

    @When("In dropdown from options column from {string} access and select {string}")
    public void inDropdownFromOptionsColumnFromAccessAndSelect(String access, String button) throws IOException {
        siteName = basePage.sitePage().getAccessSiteName(access);
        basePage.sitePage().dropDownAccess(access);
        basePage.sitePage().clickOnButton(button);
    }

    @Then("Verify that for Active sites there should be option to deactivate site")
    public void verifyThatForActiveSitesThereShouldBeOptionToDeactivateSite() throws IOException {
        basePage.sitePage().verifyOnSpan("Deactivate");
    }

    @When("In dropdown from options column select Activate")
    public void inDropdownFromOptionsColumnSelectActivate() throws IOException, InterruptedException {
        siteName = basePage.sitePage().getTheSiteNameForDeactivated();
        basePage.sitePage().clickOnDeactivatedOption();
        basePage.sitePage().clickOnButton("Activate");
        Thread.sleep(3000);
    }

    @Then("Verify that for deactivated sites there should be option to make it active again")
    public void verifyThatForDeactivatedSitesThereShouldBeOptionToMakeItActiveAgain() throws IOException {
        Assert.assertEquals(basePage.dashboardPage().getSiteTitle(siteName), 0);
    }

    @And("Click on Replace site image and browse image from the system")
    public void clickOnReplaceSiteImageAndBrowseImageFromTheSystem() throws InterruptedException, IOException, AWTException {
        Thread.sleep(4000);
        basePage.sitePage().uploadDisplayImageReplace(path + "image4.jpg");
    }

    @When("Click on Remove Site Image and browse image from the system")
    public void clickOnRemoveSiteImageAndBrowseImageFromTheSystem() throws InterruptedException, IOException, AWTException {
        Thread.sleep(4000);
        basePage.sitePage().removeSiteImage(path + "image4.jpg");
    }

    @Then("the image should not be added a background image on top of site dashboard")
    public void theImageShouldNotBeAddedABackgroundImageOnTopOfSiteDashboard() throws IOException, InterruptedException {
        Thread.sleep(2000);
        Assert.assertEquals(basePage.sitePage().verifyOnCoverUploadImage(),0);
    }

    @And("click on the upload button and if image is there then remove and then upload")
    public void clickOnTheUploadButtonAndIfImageIsThereThenRemoveAndThenUpload() throws IOException, AWTException {
        basePage.sitePage().uploadCoverImageRemove(path +"image4.jpg");
    }

    @And("Upload invalid image display image on site dashboard")
    public void uploadInvalidImageDisplayImageOnSiteDashboard() throws InterruptedException, IOException, AWTException {
        Thread.sleep(5000);
        basePage.sitePage().uploadDisplayImage(path + "testData.txt");
    }

    @Then("Verify that application should allow to upload another file format than image")
    public void verifyThatApplicationShouldAllowToUploadAnotherFileFormatThanImage() throws IOException {
        Assert.assertTrue(basePage.homePage().getMessage("Could not add file - image extension should be png")>1);
    }

    @And("click on upload and send the invalid file")
    public void clickOnUploadAndSendTheInvalidFile() throws InterruptedException, IOException, AWTException {
        Thread.sleep(5000);
        basePage.sitePage().uploadCoverImage(path + "testData.txt");
    }

    @And("Click on remove cover image")
    public void clickOnRemoveCoverImage() throws IOException, AWTException {
        basePage.sitePage().removeCoverImage(path +"image4.jpg");
    }

    @Then("Verify that user should be able to see list of feed which are marked as favorite and sort the list")
    public void verifyThatUserShouldBeAbleToSeeListOfFeedWhichAreMarkedAsFavoriteAndSortTheList() throws IOException, ParseException {
        Assert.assertTrue(basePage.sitePage().compareLatestDate());
    }

    @Given("Click on Content Icon on Home page")
    public void clickOnContentIconOnHomePage() throws IOException {
        basePage.sitePage().clickOnAddContentOnHome();
    }

    @And("Select site from the drop down {string} on home page")
    public void selectSiteFromTheDropDownOnHomePage(String site) throws IOException {
        basePage.sitePage().chooseSiteFromDropDown(site);
    }

    @And("Click on follow button on site page")
    public void clickOnFollowButtonOnSitePage() throws IOException {
        basePage.sitePage().clickOnFollowButton(siteName);
    }

    @When("Click on unfollow button on site page")
    public void clickOnUnfollowButtonOnSitePage() throws IOException {
        basePage.sitePage().clickOnUnfollowButton(siteName);
    }

    @When("Click on request Membership")
    public void clickOnRequestMembership() throws IOException {
        basePage.sitePage().clickOnRequested();
    }

    @Given("Get the recently visited site name")
    public void getTheRecentlyVisitedSiteName() throws IOException, InterruptedException {
        List<String> siteList = basePage.sitePage().recentlyVisitedSitesCount();
        for (String s : siteList) {
            if (!s.equals("dosli_62Site_Name") || !s.equals("blogger")) {
                siteName = s;
                break;
            }
        }
    }


    @And("Click on option button for the recently visited site")
    public void clickOnOptionButtonForTheRecentlyVisitedSite() throws IOException {
        basePage.profileMenuPage().clickOnDropDownOnManageSite(siteName);
    }

    @Then("Verify deactivated site should not be there in the recently visited sites")
    public void verifyDeactivatedSiteShouldNotBeThereInTheRecentlyVisitedSites() throws IOException, InterruptedException {
        Assert.assertFalse(basePage.sitePage().recentlyVisitedSitesCount().contains(siteName));
    }

    @Then("Application should allow to {string} and {string} file")
    public void applicationShouldAllowToAndFile(String first, String second) throws IOException {
        if(basePage.sitePage().getListOfButton().contains(second)){
            basePage.sitePage().clickOnButton(second);
            Assert.assertTrue(basePage.sitePage().getListOfButton().contains(first));
            basePage.sitePage().clickOnButton(first);
            Assert.assertTrue(basePage.sitePage().getListOfButton().contains(second));
        }else{
            basePage.sitePage().clickOnButton(first);
            Assert.assertTrue(basePage.sitePage().getListOfButton().contains(second));
            basePage.sitePage().clickOnButton(second);
            Assert.assertTrue(basePage.sitePage().getListOfButton().contains(first));
        }
    }

    @Then("Application should allow to preview file which shows all details {string} {string} {string} {string} {string} and {string}")
    public void applicationShouldAllowToPreviewFileWhichShowsAllDetailsAnd(String param1, String param2, String param3, String param4, String param5, String param6) throws IOException {
        Assert.assertTrue(basePage.sitePage().verifyOnDT(param1));
        Assert.assertTrue(basePage.sitePage().verifyOnDT(param2));
        Assert.assertTrue(basePage.sitePage().verifyOnDT(param3));
        Assert.assertTrue(basePage.sitePage().verifyOnDT(param4));
        Assert.assertTrue(basePage.sitePage().verifyOnDT(param5));
        Assert.assertTrue(basePage.sitePage().verifyOnDT(param6));
    }

    @And("Enter person name {string} in the input box")
    public void enterPersonNameInTheInputBox(String name) throws IOException, InterruptedException {
        basePage.profileMenuPage().sendPersonName(name);
    }

    @And("Click on Add Person button")
    public void clickOnAddPersonButton() throws IOException {
        basePage.profileMenuPage().clickOnAddPerson();
    }

    @And("Get the user name on followers tab")
    public void getTheUserNameOnFollowersTab() throws IOException {
        nameTitle = basePage.sitePage().getTheUSerName().get(0);
    }

    @Then("Application should allow to change the role of follower to site manager")
    @Then("Application should allow to change the role of member to follower")
    public void applicationShouldAllowToChangeTheRoleOfFollowerToSiteManager() throws IOException {
        Assert.assertTrue(basePage.homePage().verifyLink(nameTitle));
    }

    @Then("Application should allow to remove the role of person")
    public void applicationShouldAllowToRemoveTheRoleOfPerson() throws IOException, InterruptedException {
        Thread.sleep(3000);
        Assert.assertFalse(basePage.homePage().verifyLink(nameTitle));
    }

    @Then("Application should show list of followers to the site")
    @Then("Application should show list of Members to the site")
    @Then("Application should show list of Owner & managers to the site")
    public void applicationShouldShowListOfFollowersToTheSite() throws IOException {
        Assert.assertTrue(basePage.sitePage().getTheUSerName().size()>0);
    }

    @And("Click on member radio button")
    public void clickOnMemberRadioButton() throws IOException {
        basePage.sitePage().clickOnMemberRadioButton();
    }

    @Then("User should not see {string} option")
    public void userShouldNotSeeOption(String name) throws IOException {
        Assert.assertEquals(basePage.profileMenuPage().verifyValueFromTheLink(name),0);
    }
}