package StepDefinition;

import com.simpplr.zeus.automation.base.Constants;
import com.simpplr.zeus.automation.util.ReadProperty;
import io.cucumber.java.Before;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.simpplr.zeus.automation.page.BasePage;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;

import java.io.IOException;
import java.util.Properties;

public class CommonStepDef {
    public static BasePage basePage = new BasePage();
    private static final Logger logger = LogManager.getLogger(CommonStepDef.class);
 @Given("Login as {string}")
    public void loginAs(String user) throws IOException {
        Properties pro = ReadProperty.readPropertiesFile(Constants.credentials);
        String env = pro.getProperty("Env");
        if (env.equalsIgnoreCase("QA")) {
            basePage.getDriver().get(pro.getProperty("URL_QA"));
            if (user.equalsIgnoreCase("Admin")) {
                basePage.loginPage().login(pro.getProperty("USERNAME_QA"), pro.getProperty("PASSWORD_QA"));
            } else if (user.equalsIgnoreCase("EndUser")) {
                basePage.loginPage().login(pro.getProperty("END_USERNAME_QA"), pro.getProperty("END_PASSWORD_QA"));
            }
        } else if (env.equalsIgnoreCase("Dev")) {
            basePage.getDriver().get(pro.getProperty("URL_DEV"));
            if (user.equalsIgnoreCase("Admin")) {
                basePage.loginPage().login(pro.getProperty("USERNAME_DEV"), pro.getProperty("PASSWORD_DEV"));
            } else if (user.equalsIgnoreCase("EndUser")) {
                basePage.loginPage().login(pro.getProperty("END_USERNAME_DEV"), pro.getProperty("END_PASSWORD_DEV"));
            }
        } else if (env.equalsIgnoreCase("E2E")) {
            basePage.getDriver().get(pro.getProperty("URL_E2E"));
            if (user.equalsIgnoreCase("Admin")) {
                basePage.loginPage().login(pro.getProperty("USERNAME_E2E"), pro.getProperty("PASSWORD_E2E"));
            } else if (user.equalsIgnoreCase("EndUser")) {
                basePage.loginPage().login(pro.getProperty("END_USERNAME_E2E"), pro.getProperty("END_PASSWORD_E2E"));
            }
        }

    }

    @Before
    public void initializeTest() {
        basePage.getDriverFromPageInit();
    }

    @SuppressWarnings("deprecation")
	@After
    public void endTest(Scenario scenario) {
        if (scenario.isFailed()) {

            try {
                if (scenario.isFailed()) {
                    final byte[] screenshot = ((TakesScreenshot) basePage.getDriver())
                            .getScreenshotAs(OutputType.BYTES);
                    scenario.embed(screenshot, "image/png");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            logger.info(scenario.getName() + " is pass");
        }

        basePage.getDriver().quit();
    }
}
