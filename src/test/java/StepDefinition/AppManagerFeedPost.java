package StepDefinition;

import com.simpplr.zeus.automation.page.BasePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AppManagerFeedPost {

    public static BasePage basePage = new BasePage();

    @And("Enter the text under the reply input box")
    public void enterTheTextUnderTheReplyInputBox() {
    }

    @Then("Verify that application should allow to reply and visible as comment to the post")
    public void verifyThatApplicationShouldAllowToReplyAndVisibleAsCommentToThePost() {
        
    }

    @When("Click on favourite icon next to options on feed post to untag from favorite")
    public void clickOnFavouriteIconNextToOptionsOnFeedPostToUntagFromFavorite() {
        
    }

    @Then("Verify that application should allow to untag post as favourite")
    public void verifyThatApplicationShouldAllowToUntagPostAsFavourite() {
        
    }

    @When("Check default sorting order")
    public void checkDefaultSortingOrder() {
        
    }

    @Then("Verify that default sorting order of feed post")
    public void verifyThatDefaultSortingOrderOfFeedPost() {
        
    }

    @When("Click on Sort By option and select recent activity")
    public void clickOnSortByOptionAndSelectRecentActivity() {
        
    }

    @Then("Verify that feed should be sort by recent activity")
    public void verifyThatFeedShouldBeSortByRecentActivity() {
    }

    @When("Click on Sort By option and select post date")
    public void clickOnSortByOptionAndSelectPostDate() {
    }

    @Then("Verify that feed should be sort by post date")
    public void verifyThatFeedShouldBeSortByPostDate() {
    }

    @Then("Verify that user should able to view all the feeds to the site which includes individual posts and the thread, comments on the post")
    public void verifyThatUserShouldAbleToViewAllTheFeedsToTheSiteWhichIncludesIndividualPostsAndTheThreadCommentsOnThePost() {
    }

    @And("Select  post added by the user and hover on it")
    public void selectPostAddedByTheUserAndHoverOnIt() {
    }

    @And("Click on Option and select Edit")
    public void clickOnOptionAndSelectEdit() {
    }

    @And("Update the entered text")
    public void updateTheEnteredText() {
    }

    @When("Click on Update button")
    public void clickOnUpdateButton() {
    }

    @Then("Verify that user should able to update feed post")
    public void verifyThatUserShouldAbleToUpdateFeedPost() {
    }

    @Then("Verify that application should show text editor when user selects edit option")
    public void verifyThatApplicationShouldShowTextEditorWhenUserSelectsEditOption() {
    }

    @When("Click on Cancel button")
    public void clickOnCancelButton() {
    }

    @Then("Verify application should edit operation when user click on cancel button")
    public void verifyApplicationShouldEditOperationWhenUserClickOnCancelButton() {
    }
}
