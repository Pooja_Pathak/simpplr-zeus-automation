package Runner;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.runner.RunWith;
import org.testng.annotations.AfterClass;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.presentation.PresentationMode;
import net.masterthought.cucumber.sorting.SortingMethod;


@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/java/Features"},
                     plugin = {"pretty", "json:target/reports/cucumber.json","junit:target/reports/cucumber.xml",
                    		 "html:target/reports","de.monochromata.cucumber.report.PrettyReports:target/maven-cucumber-report"},
                strict=true,tags= {"@smoke"},
                 glue = {"StepDefinition"})
public class Runner 
{
	
	@SuppressWarnings("unchecked")
	@AfterClass
	public static void tearDown() {
	
		File outputDirectory=new File("target/maven-cucumber-report");
		List jsonFiles=new ArrayList<>();
		jsonFiles.add("target/reports/cucumber.json");
		Configuration config=new Configuration(outputDirectory,"Zeus UI Automation");
		config.setBuildNumber(config.getBuildNumber());
		config.addClassifications("Environment", "QA");
		config.addClassifications("Browser", "Edge");
		config.addClassifications("Project","Zeus UI Automation");
		config.addClassifications("Platform", System.getProperty("os.name").toUpperCase());
		config.setSortingMethod(SortingMethod.NATURAL);
		config.addPresentationModes(PresentationMode.EXPAND_ALL_STEPS);
		config.setTrendsStatsFile(new File("target/test-classes/demo-trends.json"));
		ReportBuilder rep=new ReportBuilder(jsonFiles,config);
		rep.generateReports();
		
	}
	
	
    
}
