Feature: App Manager Sites

  Background:
    Given Login as "Admin"
    And Click on "Sites" tab
    And Click on "All sites" link

  @SamidhaC
  Scenario: 116 Site Dashboard Reply to feed post Reply to the post
    Given Click on "My sites" link
    And Click on "dosli_62Site_Name" site
    And Click on "feed" link
    And Click on "Reply" button
    And Enter the text under the reply input box
    When Click on "Reply" button
    Then Verify that application should allow to reply and visible as comment to the post


  @SamidhaC
  Scenario: 117 Site Dashboard Add favourite post
    Given Click on "1" site link
    And Click on "feed" link
    When Click on favourite icon next to options on feed post
    Then Verify that user should be able to add global feed post as favourite

  @SamidhaC
  Scenario: 118 Site Dashboard UnTag favourite post
    Given Click on "1" site link
    And Click on "feed" link
    When Click on favourite icon next to options on feed post to untag from favorite
    Then Verify that application should allow to untag post as favourite

  @SamidhaC
  Scenario: 119 Site Dashboard Sort feed post default sorting
    Given Click on "1" site link
    And Click on "feed" link
    When Check default sorting order
    Then Verify that default sorting order of feed post

  @SamidhaC
  Scenario: 120 Site Dashboard Sort feed post by recent activity
    Given Click on "1" site link
    And Click on "feed" link
    When Click on Sort By option and select recent activity
    Then Verify that feed should be sort by recent activity

  @SamidhaC
  Scenario: 121 Site Dashboard Sort feed post by post date
    Given Click on "1" site link
    And Click on "feed" link
    When Click on Sort By option and select post date
    Then Verify that feed should be sort by post date

  @SamidhaC
  Scenario: 122 Site Dashboard Feed post list
    Given Click on "1" site link
    When Click on "feed" link
    Then Verify that user should able to view all the feeds to the site which includes individual posts and the thread, comments on the post

  @SamidhaC
  Scenario: 123 Site Dashboard Update feed post
    Given Click on "1" site link
    And Click on "feed" link
    And Select  post added by the user and hover on it
    And Click on Option and select Edit
    And Update the entered text
    When Click on Update button
    Then Verify that user should able to update feed post

  @SamidhaC
  Scenario: 124 Site Dashboard Update feed post Text editor
    Given Click on "1" site link
    And Click on "feed" link
    And Select  post added by the user and hover on it
    When Click on Option and select Edit
    Then Verify that application should show text editor when user selects edit option

  @SamidhaC
  Scenario: 125 Site Dashboard Update feed post Cancel button
    Given Click on "1" site link
    And Click on "feed" link
    And Select  post added by the user and hover on it
    And Click on Option and select Edit
    And Update the entered text
    When Click on Cancel button
    Then Verify application should edit operation when user click on cancel button
