Feature: End User Sites

  Background:
    Given Login as "EndUser"
    And Click on "Sites" tab
    And Click on link "All sites" with index "1"

  @SonaliG @smoke
  Scenario: 164 Add Content Publish Page Submit for approval
    Given Click on link "My sites" with index "1"
    And Click on link "dosli_62Site_Name" with index "1"
    And Click on Add Content icon
    And select the Add content label as "page"
    And Click on Add button
    And Enter all details for creating page with category "uncategorized" and access or content "News"
    When Click on "Publish" button and index "1"
    Then Verify that application show Submit for approval button when logged user created content page
