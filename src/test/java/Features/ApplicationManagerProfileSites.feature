Feature: End User Sites

  Background:
    Given Login as "EndUser"
    And Click on "Sites" tab
    And Click on "All sites" link

  @SonaliG @smoke
  Scenario: 163 Add Content Publish Page Submit for approval
    Given Click on "1" site link
    And Click on Add Content icon
    And select the Add content label as "page"
    And Click on Add button
    And Enter all details for creating page with category "uncategorized" and access or content "News"
    When Click on "Submit for approval" button
    Then Verify that application show Submit for approval button when logged user created content page

  Scenario: 020 Verify that application should show list of site as per the My Sites
    When Click on "My sites" link
    Then Verify that application should not show list of site

  Scenario: 023 Verify that application should show list of site as per the latest
    When Click on "Latest" link
    Then Application should not show list of site under Latest tab

  Scenario: 099 Site Dashboard Follow
    When Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    When Click on follow button on site page
    Then Site Page should show "Following" button on the right corner of the page

  Scenario: 100 Site Dashboard UnFollow
    When Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    When Click on unfollow button on site page
    Then Site Page should show "Follow" button on the right corner of the page

  Scenario: 100 Site Dashboard UnFollow
    When Click on "My sites" link
    And Search site "Staged_Site" on my site page
    When Click on request Membership
    Then Site Page should show "Access requested" button on the right corner of the page