Feature: App Manager Home Dashboard

  Background:
    Given Login as "Admin"
    And Click on "Home" tab

  @SonaliG
  Scenario: 001 Verify that Sites tab should be present on Homepage and on clicking All sites option should show
    When Click on "Sites" tab
    Then User should see "All sites" option

  @SonaliG @smoke
  Scenario: Login_001 Login to Tenant Application successfully As App Manager
    Then Verify that user should be logged in successfully aa App Manager

  @SonaliG @smoke
  Scenario: 225 Verify that user should be able to see add tile and manage dashboard button on homepage
    Then Verify that user should be able to see add tile and manage dashboard button on homepage

  @SonaliG @smoke
  Scenario: 226 266 Homepage: Add tile popup
    When Click on add tile
    Then Verify that user should have options to add tile on homepage with popup
    And Verify that when user add tile on home dashboard there should be option to add content to all user home on add tile popup

  @SonaliG @smoke
  Scenario: 227 Homepage: Add tile with content page
    Given Click on add tile
    And Click on "Add pages" paragraph
    And Enter information on Add tiles
    When Click on "Add to home" button
    Then Verify that user should be able to add content on homepage

  @SonaliG @smoke
  Scenario: 240 Global feed post: add as favourite
    Given Check Feed tab is enabled
    When Click on favourite icon next to options on feed post
    Then Verify that user should be able to add global feed post as favourite

  @SonaliG @smoke
  Scenario: 235 Global feed post: like post
    Given Check Feed tab is enabled
    When Click on like button on post
    Then Verify that user should be able to like the post on global feed post

  @SonaliG @smoke
  Scenario: 236 Global feed post:  replies post
    Given Check Feed tab is enabled
    When Click on "Reply" button
    Then Verify that user should be able to create replies for index "1" on global feed post which are accessible to user

  @SonaliG @smoke
  Scenario: 231 Global feed post: create post
    Given Check Feed tab is enabled
    And Click on "Share your thoughts or question" button
    When Create a post and send it to the editor
    And Click on "Post" button
    Then Verify that user should be able to create a global post

    @SonaliG
    Scenario: 215 Quill Editor
      Given Click on Content Icon on Home page
      And select the Add content label as "page"
      And Select site from the drop down "blogger" on home page
      When Click on Add button
      And Enter all details for creating page with category "uncategorized" and access or content "News"
      When Click on "Publish" button
      Then Verify that user should be able to publish the created Add Content

      @SonaliG
      Scenario: 212 Content: Pending/drafted via My content
        And Click on Avatar from profile menu
        And Click on "My content" link
        Then Verify that user should be able to view the content which are in draft or pending state via Manage site