Feature: Home Dashboard

  Background: 
    Given Login as "Admin"
    And Click on "Home" tab


  Scenario: Site_UI_3199 Site_UI_249 Site_UI_250 Verify that user with proper permission should be able to add proper layout by selecting the layout from dashboard settings
    Given Click on dashboard setting option on bottom right corner of screen
    And Select the layout
    When Click Done
    Then Application layout should change as selected by user without any error
    
 
    Scenario: Site_UI_246 Site_UI_244  Site_UI_245
    Given Click on Add tile option on bottom right corner of screen
    When Select site category tile and enter detail
    And Select site category tab
    And Click Add to home button
    Then Application should allow user to add tiles
 
 
    
    Scenario: Site_UI_247 Site_UI_248
    Given Click on Add tile option on bottom right corner of screen
    When Select site category tile and enter detail
    And Select site category tab
    And Click Add to home button
    Then Application should allow user to add tiles
    Given Click on Add tile option on bottom right corner of screen
    When Select site category tile and enter detail
    And Select site category tab
    And Click Add to home button
    Then Application should allow user to add tiles
    Given Click on Site Category Option
    When Click on "Remove" option
    And Again click on remove on warning popup 
    Then Application should remove tile from dashboard
    
  @xyzp
    Scenario: Site_UI_228 Site_UI_229 Site_UI_230
    Given Click on Add tile option on bottom right corner of screen
    And Select the tile and fill details
    And Select Pages from category tab and fill all the information
    And Enter Title Name
    And Click Add to home button
    Then Application should allow user to add tiles
    And Click on Content Option
    When Click on "Edit" option
    And Click on Save  
    Then Application should edit tile successfully
    And Click on "Home" tab
    And Click on Content Option
    When Click on "Remove" option
    And Again click on remove on warning popup 
    And Application should remove tile from dashboard
     
    
    
    
      