Feature: App Manager Sites

  Background:
    Given Login as "Admin"
    And Click on "Sites" tab
    And Click on Avatar from profile menu
    And Click on Favourites from profile menu

  @SonaliG
  Scenario: 203 Favorite view sites
    Then Verify that user should be able to view the site which is marked as favorite

  @SonaliG
  Scenario: 204 Favorite Search site
    When Search the site from search box on favorite site
    Then Verify that user should be able to search the favorite site from search option

  @SonaliG
  Scenario: 205 Favorite cancel Search site
    Given Search the site from search box on favorite site
    When Click on clear search or cross button
    Then Verify that user should be able to view the site which is marked as favorite

  @SonaliG
  Scenario: 206 Favorite view content
    When Click on "Content" link
    Then Verify that user should be able to view the site which is marked as favorite

  @SonaliG
  Scenario: 207 Favorite view content with filter
    Given Click on "Content" link
    When Click on "Pages" link
    Then Verify that user should be able to see list of content which are marked as favorites and also should be able to apply filter


  @SonaliG
  Scenario: 208 Favorite view Feed
    When Click on "Feed posts" link
    Then Verify that user should be able to see list of feed which are marked as favorite

  @SonaliG
  Scenario: 209 Favorite view filtered Feed
    Given Click on "Feed posts" link
    Then Verify that user should be able to see list of feed which are marked as favorite and sort the list