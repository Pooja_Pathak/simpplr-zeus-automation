Feature: App Manager Sites

  Background:
    Given Login as "Admin"
    And Click on "Sites" tab
    And Click on "All sites" link

  @SonaliG @smoke
  Scenario: 002 003 011 Verify site page after clicking on All sites option from sites tab
    Then User should landed on Sites page
    And Site Page should show "Add site" button on the right corner of the page
    And Site Dashboard page should show list of categories

  @SonaliG @smoke
  Scenario: 004 Verify Add Site form
    When Click on "Add site" button
    Then Add site form should be displayed

  @SonaliG @smoke
  Scenario: 005 Add Site with existing category
    Given Click on "Add site" button
    And Enter all details of site with category "uncategorized" and access or content "Private"
    When Click on "Add site" button
    Then Site should be saved successfully with uncategorized category

  @SonaliG @smoke
  Scenario: 006 Add Site with new category
    Given Click on "Add site" button
    And Enter all details of site with random category and access or content "Private"
    When Click on "Add site" button
    Then Site should be saved successfully and newly created site populated on the site page

  @SonaliG @smoke
  Scenario: 007 Add Site with new category and cancel
    Given Click on "Add site" button
    And Enter all details of site with random category and access or content "Private"
    When Click on cancel button on add site pop up
    Then Site should not be created after cancel site details

  @SonaliG @smoke
  Scenario: 008 Verify site dashboard
    Given  Click on "1" site link
    When Click on "Dashboard" link
    Then Dashboard page should show the site name and category as selected by owner or user with site management permission

  @SonaliG
  Scenario: 009 Site Image Upload functionality
    Given  Click on "1" site link
    And Click on Upload Image button and browse image from the system
    When Click on "Crop" button
    Then Image should be added as a background image on top of site dashboard

  @SonaliG
  Scenario: 147 Site Dashboard Invalid file
    Given  Click on "1" site link
    And Upload invalid image display image on site dashboard
    Then Verify that application should allow to upload another file format than image

  @SonaliG
  Scenario: 146 Site Dashboard Replace site image
    Given  Click on "1" site link
    And Click on Replace site image and browse image from the system
    When Click on "Crop" button
    Then Image should be added as a background image on top of site dashboard

  @SonaliG
  Scenario: 148 Remove site Image
    Given  Click on "1" site link
    When Click on Remove Site Image and browse image from the system
    Then Verify the cancel button functionality for the site image upload

  @SonaliG
  Scenario: 010 Site Image Upload cancel functionality
    Given  Click on "1" site link
    And Click on Upload Image button and browse image from the system
    When Click on button "Cancel"
    Then Verify the cancel button functionality for the site image upload

  @SonaliG
  Scenario: 012 Verify Categories page from all site page
    When Click on View All link under categories section
    Then Application should show "Categories" tab is active
    And Application should redirected to categories page and shows list of all categories and the count of sites linked with the category

  @SonaliG
  Scenario: 013 Verify that application should throw proper error message for duplicate site name
    Given Get existing site name from recent visited sites at index "1"
    And Click on "Add site" button
    And Enter all details of site with category "uncategorized" and access or content "Private" and duplicate site name
    When Click on "Add" button
    Then Application should throw error for duplicate site name

  @SonaliG
  Scenario: 014 Verify that application should show list of site as per the site categories
    When Click on "Categories" link
    Then Application should show "Categories" tab is active
    And Verify that application should show list of site as per the site categories

  @SonaliG
  Scenario: 015 025 028 027 Verify that application should show list of  featured site
    When Click on "Featured" link
    Then Application should list of featured sites
    And Application should show 4 recently visited sites
    And Verify that application should show recently visited sites "dosli_62Site_Name" on Featured site
    And Verify that application should not show recently visited sites "xyz" on Featured site
    And Verify that application should not show "xyz" as Featured site

  @SonaliG
  Scenario: 016 018 029 035 Verify that application should allow to add site as featured site
    Given Click on "Featured" link
    And Click on "Add featured site" button
    When Enter name of the site from the recently visited site and select from dropdown
    Then Application should add site a as featured site and populated in the featured site category
    And Application should  show Featured cards with site name, site logo, category

  Scenario: 017 Verify that application should not show non existing site in the dropdown list
    Given Click on "Featured" link
    And Click on "Add featured site" button
    When Enter name of the random site and select from dropdown
    Then Application should not show site name in the dropdown list as it is not existing

  @SonaliG
  Scenario: 019 021 Verify that application should show list of site as per the My Sites
    When Click on "My sites" link
    Then Application should show list of sites under My Sites tab
    And Application should show "My Sites" tab is active

  @SonaliG
  Scenario: 036 037 039 048 Verify that user is able to click following tab
    When Click on "Following" link
    Then Application should show "Following" tab is active
    And Application should show followed sites with name, category and members and following button
    And Application should not list sites not followed by user

  @SonaliG
  Scenario: 022 024 Verify that application should show list of site as per the latest
    When Click on "Latest" link
    Then Application should show list of sites under Latest tab
    And Application should show "Latest" tab is active
    And Verify that application should show list of my sites

  @SonaliG
  Scenario: 026 Verify that after clicking on site shown in recently visited sites redirect to that site
    Given Click on "Featured" link
    When Click on "1" site link
    Then Application should redirect to site without any error

  @SonaliG
  Scenario: 030 Verify that after clicking on Featured card’s photo or Name application should redirect that site
    Given Click on "Featured" link
    And Click on "Add featured site" button
    And Enter name of the site from the recently visited site and select from dropdown
    When Click on Featured card photo or Name
    Then Application should redirect to site without any error

  @SonaliG
  Scenario: 031 Verify that after clicking on categories link user should redirect to categories tab
    Given Click on "Featured" link
    And Click on "Add featured site" button
    And Enter name of the site from the recently visited site and select from dropdown
    When Click on Featured card categories link
    Then Application should show "Categories" tab is active

  @SonaliG
  Scenario: 032 Verify that after clicking on members link user should redirect to members list
    Given Click on "Featured" link
    And Click on "Add featured site" button
    And Enter name of the site from the recently visited site and select from dropdown
    When Click on Featured card Members link
    Then Application should redirect to members list without any error on site page

  @SonaliG
  Scenario: 038 Verify that after clicking follow button in following tab user should be able to unfollow the site
    Given Click on "Following" link
    When Click on "Following" button
    Then Application should allow user to unfollow site

  @SonaliG
  Scenario: 040 Verify that Pagination is added in following tab
    Given Click on "Following" link
    Then Application should have pagination and after clicking show more it should show rest of sites as user scroll down

  @SonaliG
  Scenario: 041 Verify that user should be able to search sites based on site name
    Given Click on "Following" link
    When Enter the string user want to search
    Then Application should allow user to search sites

  @SonaliG
  Scenario: 042 Verify that if searched site is not present UI should give proper error.
    Given Click on "Following" link
    When Enter the not followed string user want to search
    Then Application should throw proper error

  @SonaliG
  Scenario: 043 044 Verify that after clicking on popular sites tab UI should show list of popular sites.
    When Click on "Popular" link
    Then Application should show list of popular sites
    And Application should show most visited "dosli_62Site_Name"

  @SonaliG
  Scenario: 150 149 Verify that application should show add and setting button image as logged user has view site permission
    Given  Click on "1" site link
    When Click on "Dashboard" link
    Then Application should view uploaded the site image and site cover image uploaded
    And Verify that application should show add and setting button image as logged user has application manager or site manager

  @smoke @SonaliG
  Scenario: 166 168 Verify the functionality of site image upload
    Given  Click on "1" site link
    And Click on Upload Image on cover and browse image from the system
    When Click on "Crop" button
    Then the image should be added as a background image on top of site dashboard

  @SonaliG
  Scenario: 167 Verify the functionality of cover image upload
    Given  Click on "1" site link
    And click on the upload button and if image is there then remove and then upload
    When Click on button "Cancel"
    Then the image should not be added a background image on top of site dashboard

  @SonaliG
  Scenario: 170 Site Dashboard - Remove cover image
    Given  Click on "1" site link
    And Click on remove cover image
    Then the image should not be added a background image on top of site dashboard

  @SonaliG
  Scenario: 169 Site Dashboard - Replace cover image - Upload another format file
    Given  Click on "1" site link
    And click on upload and send the invalid file
    Then Verify that application should allow to upload another file format than image

  @smoke @SonaliG
  Scenario: 213 Verify that user should be able to like/unlike page on site
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on "Content" link
    When Click on "1" page
    And Click on like or unlike button at bottom of page
    Then Verify the content page like dislike status

  @smoke @SonaliG
  Scenario: 199 Carousel:  Add carousel popup
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on "Dashboard" link
    And Click on setting option
    When Click on button "Carousel"
    Then Verify that there should be a option to add carousel on UI

  @smoke @SonaliG
  Scenario: 200 201 202 Carousel: search from content
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on "Dashboard" link
    And Click on setting option
    And Click on button "Carousel"
    When Search for content in search field
    Then Verify that user should be able to search the content to add it in the carousel

  @smoke @SonaliG
  Scenario: 216 Verify that user should have option to select from 2 carousel layouts standard and showcase
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on "Dashboard" link
    And Click on setting option
    And Click on button "Carousel"
    When Select layout "Showcase" from carousel tab
    And Click on "Done" button
    Then Verify that user should have option to select carousel layouts as "Showcase"

  @SonaliG @smoke
  Scenario: 113 Site Dashboard Reply to feed post
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on "Dashboard" link
    When Click on "Reply" button
    Then Verify that user should be able to create replies for index "1" on global feed post which are accessible to user

  @SonaliG @smoke
  Scenario: 105 Site Dashboard - Feed Post
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on "Dashboard" link
    And Click on "Share your thoughts or question" button
    When Create a post and send it to the editor
    And Click on "Post" button
    Then Verify that user should be able to create a global post

  @SonaliG @smoke
  Scenario: 151 Page Content Add Page Click on Add Content
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    When Click on Add Content icon
    Then Verify that application should pop up for Add content site with label "page" ,"event", "album"

  @SonaliG @smoke
  Scenario: 152 Page Content Add  Page
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on Add Content icon
    And select the Add content label as "page"
    And Select site from the drop down "blogger"
    When Click on Add button
    Then Verify that application should add page to the selected site

  @SonaliG @smoke
  Scenario: 154 Page Content Add Page Select site from recent used site
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on Add Content icon
    And select the Add content label as "page"
    When Click on Add button
    Then Verify that application should add page to the selected site

  @SonaliG @smoke
  Scenario: 164 165 Add Content Publish Page Submit for approval
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on Add Content icon
    And select the Add content label as "page"
    And Click on Add button
    And Enter all details for creating page with category "uncategorized" and access or content "News"
    When Click on "Save draft" button
    Then Site Page should show "Update draft" button on the right corner of the page

  @SonaliG @smoke
  Scenario: 179 Page Content Add attach file
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on Add Content icon
    And select the Add content label as "page"
    And Click on Add button
    And Click on "Select from computer" button
    When Select the file User want to attach "image1.jpg"
    And Click on Add button
    Then Verify that user should be able to upload any file properly

  @SonaliG @smoke
  Scenario: sAWS_4131 Page Content- Add attach file with all the Mandatory fields
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on Add Content icon
    And select the Add content label as "page"
    And Click on Add button
    And Click on "Select from computer" button
    And Select the file User want to attach "image1.jpg"
    And Click on Add button
    And Enter all details for creating page with category "uncategorized" and access or content "News"
    And Click on "Publish" button
    And Select Promote Page As "Add to site carousel"
    When Click on "Promote" button
    Then Verify that user should be able to publish the created Add Content

  @SonaliG @smoke
  Scenario: sAWS_4132 Event Content- Add attach file with all the Mandatory fields
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on Add Content icon
    And select the Add content label as "event"
    And Click on Add button
    And Click on "Select from computer" button
    And Select the file User want to attach "image1.jpg"
    And Click on Add button
    And Enter all details for creating event with location "GGN"
    And Click on "Publish" button
    And Select Promote Page As "Add to site carousel"
    When Click on "Promote" button
    Then Verify that user should be able to publish the created Add Content

  @SonaliG @smoke
  Scenario: sAWS_4133 Album Content- Add attach file with all the Mandatory fields
    Given Click on "My sites" link
    And Search site "dosli_62Site_Name" on my site page
    And Click on "dosli_62Site_Name" site
    And Click on Add Content icon
    And select the Add content label as "album"
    And Click on Add button
    And Click on "Select from computer" button
    And Select the file User want to attach "image1.jpg"
    And Enter all details for creating album
    And Click on "Publish" button
    And Select Promote Page As "Add to site carousel"
    When Click on "Promote" button
    Then Verify that user should be able to publish the created Add Content

  @SonaliG @smoke
  Scenario: 097 098 Members Tab list of sites
    When Click on "Member" link
    Then Verify that after clicking on members tab all the sites that user is part of should be listed
    And Verify that pagination is implemented on members tab.

  @SonaliG
  Scenario: 045 Drag Drop Check drag and drop on feature and category page
    Given Click on "Featured" link
    Then Verify that user should be able to drag and drop cards on these page

