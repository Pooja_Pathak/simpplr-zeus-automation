Feature: Home Site Dashboard

  Background: 
    Given Login as "Admin"
    And Click on "Sites" tab
    And Click on first available site

 
  Scenario: Site_UI_187 Site_UI_3200  Verify that user with proper permission should be able to add proper layout by selecting the layout from dashboard settings
    Given Click on dashboard setting option on bottom right corner of screen
    And Select the layout
    When Click Done
    Then Application layout should change as selected by user without any error
    
  
  Scenario: Site_UI_188 Site_UI_191 Verify that user with proper permission should be able to select a tile from 'Add Tile' popup
    Given Click on Add tile option on bottom right corner of screen
    And Select the tile and fill details
    When Click Add to site dashboard
    Then Application should allow user to add tiles

 
  Scenario: Site_UI_189 Verify that user with proper permission, should be able to drag and drop tiles in site dashboard
   Given Click on Add tile option on bottom right corner of screen
    And Select the tile and fill details
    When Click Add to site dashboard
    Given Click on Add tile option on bottom right corner of screen
    And Select the tile and fill details
    When Click Add to site dashboard
    Given UI should allow the drag and drop of a Tile


  Scenario: Site_UI_190 Site_UI_198 Site_UI_5513 Verify that user with proper permission, should be able to remove the tile from Dashboard
    Given Click on Add tile option on bottom right corner of screen
    And Select the tile and fill details
    And Enter tile name
    When Click Add to site dashboard
    Given Click on options on tile
    When Click on remove
    And Again click on remove on warning popup
    Then Application should remove tile from dashboard


  Scenario: Site_UI_192 Dashboard: Add Pages form category tile
    Given Click on Add tile option on bottom right corner of screen
    When Select the tile and fill details
    And Select Pages from category tab and fill all the information
    And Click Add to site dashboard
    Then Application should allow user to add tiles

    
  Scenario: Site_UI_193 Dashboard: Add Page category tile
    Given Click on Add tile option on bottom right corner of screen
    When Select page category tile in sites section in a popup
    And Click Add to site dashboard
    Then Application should allow user to add tiles
  

  Scenario: Site_UI_195 Favourite icon on site banner
    Given Select favourite icon
    Then Favourite icon should be selected
    Given UnSelect favourite icon
    Then Favourite icon should not be selected
 
  Scenario: Site_UI_196 Site_UI_197 Verify that user should be able add add site category tile in dashboard via popup
    Given Click on Add tile option on bottom right corner of screen
    When Select site category tile and enter detail
    And Select site category tab
    And Click Add to site dashboard
    Then Application should allow user to add tiles
    
 
@xyzp
  Scenario: Site_UI_217 Site_UI_218 Site_UI_219 Site_UI_220 Site_UI_221 Site_UI_222 
    Given Click on "Dashboard" link
    And Click on setting option
    And Click on button "Carousel"
    And Click to remove all carousel content
    And Enter Search content "Test1"
    And Enter Search content "event"
    And Click on Add carousel
    Given Click on options on carousel
    And Click on Start-Autoplay
    And Click on Stop-Autoplay
    When Select edit carousel
    And Select content and move up and down
    When Select layout "Showcase" from carousel tab
    And Click on "Done" button
    Then Verify that user should have option to select carousel layouts as "Showcase"
    And Click on options on carousel
    And Select edit carousel
    And Click to remove all carousel content
    And Click Done
    Then There is no carousel on Dashboard Application mentions empty carousel
    
  
    
    
